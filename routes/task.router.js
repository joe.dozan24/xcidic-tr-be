const express = require("express");

module.exports = function ({
    dt,
    auth,
    db,
    devModeLogging
}) {
    const router = express.Router();

    const taskModule = require("../module/task.module.js")({
        dt: dt,
        db: db,
        devModeLogging: devModeLogging
    }).instance;

    // Get Task List
    router.post("/getList/",
        auth.chkToken,
        async (req, res, next) => {
            // Pagination
            req.query.page = !isNaN(parseInt(req.query.page)) && req.query.page != '' ? parseInt(req.query.page) : undefined;
            req.query.resPerPage = !!parseInt(req.query.resPerPage) ? parseInt(req.query.resPerPage) : undefined;

            const response = await taskModule.getTaskList({
                projectId: req.body.projectId,
                statusId: req.body.statusId,
                excludedStatusId: req.body.excludedStatusId,
                divisionId: req.body.divisionId,
                searchStr: req.body.searchStr,
                onlyMine: req.body.onlyMine,
                page: req.query.page,
                resPerPage: req.query.resPerPage,
                noPaginate: req.query.page == 0 ? true : false,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );



    // Get A Task
    router.get("/get/:taskId",
        auth.chkToken,
        async (req, res, next) => {
            const response = await taskModule.getTask({
                taskId: req.params.taskId,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Create Task
    router.post("/create",
        auth.chkToken,
        async (req, res, next) => {
            const response = await taskModule.createTask({
                projectId: req.body.projectId,
                dateStart: req.body.dateStart,
                dateEnd: req.body.dateEnd,
                category: req.body.category,
                title: req.body.title,
                description: req.body.description,
                level: req.body.level,
                assignedUserId: req.body.assignedUserId,
                reviewerArr: req.body.reviewerArr,
                relatedTaskId: req.body.relatedTaskId,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Edit Task
    router.post("/edit",
        auth.chkToken,
        async (req, res, next) => {
            const response = await taskModule.editTask({
                taskId: req.body.taskId,
                dateStart: req.body.dateStart,
                dateEnd: req.body.dateEnd,
                category: req.body.category,
                title: req.body.title,
                description: req.body.description,
                level: req.body.level,
                assignedUserId: req.body.assignedUserId,
                reviewerArr: req.body.reviewerArr,
                relatedTaskId: req.body.relatedTaskId,
                isReportAccepted: req.body.isReportAccepted,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Remove Task
    router.post("/remove",
        auth.chkToken,
        async (req, res, next) => {
            const response = await taskModule.removeTask({
                taskId: req.body.taskId,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );




    // Task Category
    router.post("/category",
        auth.chkToken,
        async (req, res, next) => {
            const response = await taskModule.taskCategory({
                projectId: req.body.projectId,
                searchStr: req.body.searchStr,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Monitoring Task
    router.post("/monitoring",
        auth.chkToken,
        async (req, res, next) => {
            // Pagination
            req.query.page = !isNaN(parseInt(req.query.page)) && req.query.page != '' ? parseInt(req.query.page) : undefined;
            req.query.resPerPage = !isNaN(parseInt(req.query.resPerPage)) ? parseInt(req.query.resPerPage) : undefined;

            const response = await taskModule.monitoring({
                dateStart: req.body.dateStart,
                dateEnd: req.body.dateEnd,
                memberId: req.body.memberId,
                projectId: req.body.projectId,
                page: req.query.page,
                resPerPage: req.query.resPerPage,
                noPaginate: req.query.page == 0 ? true : false,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Get Task Statuses
    router.get("/getStatus",
        auth.chkToken,
        async (req, res, next) => {
            const response = await taskModule.getTaskStatus({
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Get Simplified Task List for Related Task
    router.get("/getSimplifiedTaskList/:projectId",
        auth.chkToken,
        async (req, res, next) => {
            const response = await taskModule.getSimplifiedTaskList({
                projectId: req.params.projectId,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );



    // Change Task Status
    /*
     * TODO
     * ONPROGRESS
     * WAITINGAPPROVAL
     * DONE
     * REPORT
     * PENDING
     * FAILED
     */
    router.post("/changeStatus",
        auth.chkToken,
        async (req, res, next) => {
            const response = await taskModule.changeTaskStatus({
                taskId: req.body.taskId,
                statusId: req.body.statusId,
                relatedTaskId: req.body.relatedTaskId,
                commentStr: req.body.comment,
                commentFile: req.file,
                extendedEndDate: req.body.extendedEndDate,
                isReportAccepted: req.body.isReportAccepted,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );


    return router;
};