const express = require("express");

module.exports = function ({
    dt,
    auth,
    db,
    devModeLogging
}) {
    const router = express.Router();

    const userModule = require("../module/user.module.js")({
        dt: dt,
        db: db,
        devModeLogging: devModeLogging
    }).instance;

    // Testing Create Superadmin Account
    router.post("/superadmin",
        async (req, res, next) => {
            const response = await userModule.superadminCreateAccount({
                userName: req.body.fullname,
                userEmail: req.body.email,
                userPassword: req.body.password
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );



    // Create User Account
    router.post("/register",
        auth.chkToken,
        async (req, res, next) => {
            const response = await userModule.registerUser({
                userEmployeeId: req.body.employeeId,
                userDivisionId: req.body.divisionId,
                userName: req.body.fullname,
                userEmail: req.body.email,
                userPassword: req.body.password,
                userPhone: req.body.phone,
                userGender: req.body.gender,
                userRole: req.body.role,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Edit User Account
    router.post("/edit",
        auth.chkToken,
        async (req, res, next) => {
            const response = await userModule.editUser({
                userId: req.body.userId,
                userEmployeeId: req.body.employeeId,
                userDivisionId: req.body.divisionId,
                userName: req.body.fullname,
                userEmail: req.body.email,
                userPhone: req.body.phone,
                userGender: req.body.gender,
                userPhoto: req.file,
                userRole: req.body.role,
                userOldPassword: req.body.oldPassword,
                userNewPassword: req.body.newPassword,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Remove User Account
    router.post("/remove",
        auth.chkToken,
        async (req, res, next) => {
            const response = await userModule.hideUser({
                userId: req.body.userId,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );



    // Login User
    router.post("/login",
        async (req, res, next) => {
            const response = await userModule.login({
                userEmployeeId: req.body.employeeId,
                userPassword: req.body.password
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Logout User
    router.post("/logout",
        auth.chkToken,
        async (req, res, next) => {
            const response = await userModule.logout({
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );



    // Get User Privileges List
    router.get("/privileges",
        auth.chkToken,
        async (req, res, next) => {
            const response = await userModule.getUserPrivilegesList({
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Get User Profile
    router.post("/profile",
        auth.chkToken,
        async (req, res, next) => {
            // Pagination
            req.query.page = !isNaN(parseInt(req.query.page)) && req.query.page != '' ? parseInt(req.query.page) : undefined;
            req.query.resPerPage = !isNaN(parseInt(req.query.resPerPage)) ? parseInt(req.query.resPerPage) : undefined;

            const response = await userModule.getUserProfile({
                userId: req.body.userId,
                roleId: req.body.roleId,
                divisionId: req.body.divisionId,
                searchStr: req.body.searchStr,
                page: req.query.page,
                resPerPage: req.query.resPerPage,
                noPaginate: req.query.page == 0 ? true : false,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Remove User Photo
    router.post("/removePhoto",
        auth.chkToken,
        async (req, res, next) => {
            const response = await userModule.removePhoto({
                userId: req.body.userId,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Forgot Password
    router.post("/forgot",
        async (req, res, next) => {
            const response = await userModule.forgotPassword({
                userEmployeeId: req.body.employeeId
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    return router;
};