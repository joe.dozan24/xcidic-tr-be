const express = require("express");

module.exports = function ({
    dt,
    auth,
    db,
    devModeLogging
}) {
    const router = express.Router();

    const projectModule = require("../module/project.module.js")({
        dt: dt,
        db: db,
        devModeLogging: devModeLogging
    }).instance;

    // Get Project List
    router.get("/getList",
        auth.chkToken,
        async (req, res, next) => {
            // Pagination
            req.query.page = !isNaN(parseInt(req.query.page)) && req.query.page != '' ? parseInt(req.query.page) : undefined;
            req.query.resPerPage = !isNaN(parseInt(req.query.resPerPage)) ? parseInt(req.query.resPerPage) : undefined;

            const response = await projectModule.getProjectList({
                searchStr: req.query.searchStr,
                page: req.query.page,
                resPerPage: req.query.resPerPage,
                noPaginate: req.query.page == 0 ? true : false,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    router.post("/getList",
        auth.chkToken,
        async (req, res, next) => {
            // Pagination
            req.query.page = !isNaN(parseInt(req.query.page)) && req.query.page != '' ? parseInt(req.query.page) : undefined;
            req.query.resPerPage = !isNaN(parseInt(req.query.resPerPage)) ? parseInt(req.query.resPerPage) : undefined;

            const response = await projectModule.getProjectList({
                searchStr: req.body.searchStr,
                page: req.query.page,
                resPerPage: req.query.resPerPage,
                noPaginate: req.query.page == 0 ? true : false,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );



    // Get A Project
    router.get("/get/:projectId",
        auth.chkToken,
        async (req, res, next) => {
            const response = await projectModule.getProject({
                projectId: req.params.projectId,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Create Project
    router.post("/create",
        auth.chkToken,
        async (req, res, next) => {
            const response = await projectModule.createProject({
                title: req.body.title,
                description: req.body.description,
                memberArr: req.body.memberArr,
                managerArr: req.body.managerArr,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Edit Project
    router.post("/edit",
        auth.chkToken,
        async (req, res, next) => {
            const response = await projectModule.editProject({
                projectId: req.body.projectId,
                title: req.body.title,
                description: req.body.description,
                memberArr: req.body.memberArr,
                managerArr: req.body.managerArr,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Remove Project
    router.post("/remove",
        auth.chkToken,
        async (req, res, next) => {
            const response = await projectModule.removeProject({
                projectId: req.body.projectId,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    return router;
};