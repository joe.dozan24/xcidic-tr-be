const express = require("express");

module.exports = function ({
    dt,
    db,
    auth,
    devModeLogging
}) {
    const router = express.Router();

    const superadminModule = require("../module/superadmin.module.js")({
        dt: dt,
        db: db,
        devModeLogging: devModeLogging
    }).instance;

    // Get Performance Member
    router.post("/performance/member",
        auth.chkToken,
        async (req, res, next) => {
            const response = await superadminModule.getPerformanceMember({
                lastXDays: req.body.lastXDays,
                isOverdue: req.body.isOverdue,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Get Performance Project
    router.get("/performance/project",
        auth.chkToken,
        async (req, res, next) => {
            const response = await superadminModule.getPerformanceProject({
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );

    // Get Change Password Requests
    router.post("/help/forgotPassword",
        auth.chkToken,
        async (req, res, next) => {
            // Pagination
            req.query.page = !isNaN(parseInt(req.query.page)) && req.query.page != '' ? parseInt(req.query.page) : undefined;
            req.query.resPerPage = !isNaN(parseInt(req.query.resPerPage)) ? parseInt(req.query.resPerPage) : undefined;

            const response = await superadminModule.getChangePasswordRequests({
                searchStr: req.body.searchStr,
                page: req.query.page,
                resPerPage: req.query.resPerPage,
                noPaginate: req.query.page == 0 ? true : false,
                decoded: req.decoded
            });

            res.setHeader('Content-type', 'application/json');
            res.status(response.statusCode).send(response);
        }
    );


    return router;
};
