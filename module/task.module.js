class __task {
    constructor({
        dt,
        db,
        devModeLogging
    }) {
        this.dt = dt;
        this.db = db;
        this.devModeLogging = devModeLogging;
    }

    // Get Task
    /**
     * 
     * @param {Object} param0 
     * @param {Int} [param0.projectId]
     * @param {Int} [param0.statusIds] - Manager Only
     * @param {Array} [param0.excludedStatusIds] - Manager Only
     * @param {Int} [param0.divisionId]
     * @param {String} [param0.searchStr]
     * @param {Int} [param0.page=1]
     * @param {Int} [param0.resPerPage=1]
     * @param {Boolean} [param0.noPaginate=false]
     * @param {Object} param0.decoded
     * @returns 
     */
    async getTaskList({
        projectId,
        statusIds = [1, 2, 3, 4, 5, 6, 7],
        excludedStatusIds = [],
        divisionId,
        searchStr = "",
        onlyMine = false,
        page = 1,
        resPerPage = 10,
        noPaginate = false,
        decoded
    }) {
        if (![2, 3].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }


        if (![2].includes(decoded.role) && !!statusIds) {
            statusIds = [1, 2, 3, 4, 5, 6, 7];
        }

        if (![2].includes(decoded.role) && !!excludedStatusIds) {
            excludedStatusIds = [];
        }

        if ([2].includes(decoded.role) && !!statusIds && !Array.isArray(statusIds)) {
            try {
                statusIds = JSON.parse(statusIds);
            }
            catch (e) {
                return {
                    statusCode: 400,
                    error: "Invalid statusIds"
                };
            }

            if (!Array.isArray(statusIds)) {
                statusIds = [statusIds];
            }
        }

        if ([2].includes(decoded.role) && !!excludedStatusIds && !Array.isArray(excludedStatusIds)) {
            try {
                excludedStatusIds = JSON.parse(excludedStatusIds);
            }
            catch (e) {
                return {
                    statusCode: 400,
                    error: "Invalid excludeStatusId"
                };
            }

            if (!Array.isArray(excludedStatusIds)) {
                excludedStatusIds = [excludedStatusIds];
            }
        }


        let tasks = await this.db.assoc.Task
            .findAll({
                attributes: [
                    "id",
                    "title",
                    "description",
                    "category",
                    "timestampStartPlan",
                    "timestampEndPlan",
                    "timestampExtendedEndPlan",
                    "timestampStartActual",
                    "timestampEndActual",
                    [this.db.sequelize.literal(
                        `CASE
                            WHEN "Task"."timestampExtendedEndPlan" IS NULL
                                THEN
                                    CASE
                                        WHEN "Task"."timestampEndActual" < "Task"."timestampEndPlan"
                                            THEN FALSE
                                        WHEN CURRENT_TIMESTAMP < "Task"."timestampEndPlan"
                                            THEN FALSE
                                        ELSE TRUE
                                    END
                                ELSE
                                    CASE
                                        WHEN "Task"."timestampEndActual" < "Task"."timestampExtendedEndPlan"
                                            THEN FALSE
                                        WHEN CURRENT_TIMESTAMP < "Task"."timestampExtendedEndPlan"
                                            THEN FALSE
                                        ELSE TRUE
                                    END
                            END`
                    ), "isOverdue"]
                ],
                include: [
                    // RefTaskLevel
                    {
                        model: this.db.assoc.RefTaskLevel,
                        attributes: [
                            "id",
                            "description"
                        ]
                    },
                    // RefTaskStatus
                    {
                        model: this.db.assoc.RefTaskStatus,
                        attributes: [
                            "id",
                            "description"
                        ]
                    },
                    // RelatedTask
                    {
                        model: this.db.assoc.Task,
                        as: "RelatedTask",
                        attributes: [
                            "id",
                            "title",
                            "isHidden"
                        ],
                        include: [
                            // TaskAssignment
                            {
                                model: this.db.assoc.TaskAssignment,
                                attributes: [
                                    "id",
                                    "RefUserPrivilegeId"
                                ],
                                include: [
                                    // User
                                    {
                                        model: this.db.assoc.User,
                                        attributes: [
                                            "id",
                                            "name"
                                        ]
                                    }
                                ],
                                where: {
                                    RefUserPrivilegeId: 3
                                },
                                required: false
                            }
                        ]
                    },
                    // Project
                    {
                        model: this.db.assoc.Project,
                        attributes: [
                            "id",
                            "name"
                        ],
                        where: (() => {
                            let obj = {};

                            if (!!parseInt(projectId)) obj.id = parseInt(projectId);

                            return obj;
                        })()
                    },
                    // CreatorUser
                    {
                        model: this.db.assoc.User,
                        as: "CreatorUser",
                        attributes: [
                            "id",
                            "name"
                        ]
                    },
                    // TaskAssignment
                    {
                        model: this.db.assoc.TaskAssignment,
                        attributes: [
                            "id",
                            "RefUserPrivilegeId"
                        ],
                        include: [
                            // User
                            {
                                model: this.db.assoc.User,
                                attributes: [
                                    "id",
                                    "name"
                                ],
                                include: [
                                    // RefDivision
                                    {
                                        model: this.db.assoc.RefDivision,
                                        attributes: [
                                            "id",
                                            "description"
                                        ],
                                        where: (() => {
                                            let obj = {};
                                            if ([2].includes(decoded.role) && !!parseInt(divisionId)) obj.id = parseInt(divisionId);
                                            return obj;
                                        })()
                                    }
                                ]
                            }
                        ],
                        where: (() => {
                            let obj = {
                                RefUserPrivilegeId: decoded.role,
                                UserId: decoded.userId
                            };

                            return obj;
                        })(),
                        required: [2].includes(decoded.role) && !onlyMine ? false : true
                    },
                ],
                where: (() => {
                    let obj = {
                        isHidden: false,
                        RefTaskStatusId: {
                            [this.db.Op.and]: {
                                [this.db.Op.in]: statusIds,
                                [this.db.Op.notIn]: excludedStatusIds
                            }
                        },
                        title: {
                            [this.db.Op.like]: "%" + searchStr + "%"
                        }
                    };

                    if ([3].includes(decoded.role)) obj.timestampStartPlan = {
                        [this.db.Op.lte]: this.db.sequelize.literal("CURRENT_DATE")
                    };

                    return obj;
                })(),
                order: [
                    ["id", "DESC"]
                ]
            })
            .catch(e => { this.devModeLogging("file:task.module func:getTaskList var:tasks", e); return []; });
        tasks = JSON.parse(JSON.stringify(tasks));

        // Paginate
        let totalResult = tasks.length;
        let totalPage = 1;
        if (!noPaginate) {
            totalPage = Math.ceil(tasks.length / resPerPage);
            tasks = tasks.splice((page - 1) * resPerPage);
            tasks.splice(resPerPage);
        }


        for (let task of tasks) {
            // (DB-R) Get reviewer list from DB using taskId
            task.ReviewerUser = await this.db.assoc.User
                .findAll({
                    attributes: [
                        "id",
                        "name",
                        [this.db.sequelize.fn("CONCAT", this.db.sequelize.col("User.id"), " - ", this.db.sequelize.col("User.name")), "nameTag"]
                    ],
                    include: [
                        {
                            model: this.db.assoc.TaskAssignment,
                            attributes: [],
                            where: {
                                RefUserPrivilegeId: 2,
                                TaskId: task.id
                            }
                        }
                    ],
                    where: {}
                })
                .catch(e => { this.devModeLogging("file:task.module func:getTaskList var:task.reviewers", e); return []; });
        }

        return {
            statusCode: 200,
            data: tasks,
            maxPage: totalPage,
            totalResult: totalResult
        };
    };

    /**
     * 
     * @param {Object} param0 
     * @param {Int} param0.taskId 
     * @param {Object} param0.decoded 
     * @returns 
     */
    async getTask({
        taskId,
        decoded
    }) {
        if (![2, 3].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Get Task from DB using taskId
        let task = await this.db.assoc.Task
            .findOne({
                attributes: [
                    "id",
                    "title",
                    "description",
                    "category",
                    "timestampStartPlan",
                    "timestampEndPlan",
                    "timestampExtendedEndPlan",
                    "timestampStartActual",
                    "timestampEndActual",
                    [this.db.sequelize.literal(
                        `CASE
                            WHEN "Task"."timestampExtendedEndPlan" IS NULL
                                THEN
                                    CASE
                                        WHEN "Task"."timestampEndActual" < "Task"."timestampEndPlan"
                                            THEN FALSE
                                        WHEN CURRENT_TIMESTAMP < "Task"."timestampEndPlan"
                                            THEN FALSE
                                        ELSE TRUE
                                    END
                                ELSE
                                    CASE
                                        WHEN "Task"."timestampEndActual" < "Task"."timestampExtendedEndPlan"
                                            THEN FALSE
                                        WHEN CURRENT_TIMESTAMP < "Task"."timestampExtendedEndPlan"
                                            THEN FALSE
                                        ELSE TRUE
                                    END
                            END`
                    ), "isOverdue"]
                ],
                include: [
                    // RefTaskStatus
                    {
                        model: this.db.assoc.RefTaskStatus,
                        attributes: [
                            "id",
                            "description"
                        ]
                    },
                    // RefTaskLevel
                    {
                        model: this.db.assoc.RefTaskLevel,
                        attributes: [
                            "id",
                            "description"
                        ]
                    },
                    // CreatorUser
                    {
                        model: this.db.assoc.User,
                        as: "CreatorUser",
                        attributes: [
                            "id",
                            "name"
                        ]
                    },
                    // TaskAssignment
                    {
                        model: this.db.assoc.TaskAssignment,
                        attributes: [],
                        where: (() => {
                            let obj = {
                                RefUserPrivilegeId: decoded.role,
                                UserId: decoded.userId
                            };

                            return obj;
                        })(),
                        required: [2].includes(decoded.role) ? false : true
                    },
                    // RelatedTask
                    {
                        model: this.db.assoc.Task,
                        as: "RelatedTask",
                        attributes: [
                            "id",
                            "title",
                            "isHidden"
                        ],
                        include: [
                            // TaskAssignment
                            {
                                model: this.db.assoc.TaskAssignment,
                                attributes: [
                                    "id",
                                    "RefUserPrivilegeId"
                                ],
                                include: [
                                    // User
                                    {
                                        model: this.db.assoc.User,
                                        attributes: [
                                            "id",
                                            "name"
                                        ]
                                    }
                                ],
                                where: {
                                    RefUserPrivilegeId: 3
                                },
                                required: false
                            }
                        ]
                    },
                ],
                where: {
                    id: taskId
                }
            })
            .catch(e => { this.devModeLogging("file:task.module func:getTask var:task", e); return null; });

        if (!task) {
            return {
                statusCode: 400,
                error: "Task not found"
            };
        }

        // (DB-R) Get reviewer list from DB using taskId
        let reviewers = await this.db.assoc.User
            .findAll({
                attributes: [
                    "id",
                    "name",
                    [this.db.sequelize.fn("CONCAT", this.db.sequelize.col("User.id"), " - ", this.db.sequelize.col("User.name")), "nameTag"]
                ],
                include: [
                    {
                        model: this.db.assoc.TaskAssignment,
                        attributes: [],
                        where: {
                            RefUserPrivilegeId: 2,
                            TaskId: taskId
                        }
                    }
                ],
                order: [
                    ["id", "ASC"]
                ]
            })
            .catch(e => { this.devModeLogging("file:task.module func:getTask var:reviewers", e); return []; });

        // (DB-R) Get member list from DB using taskId
        let members = await this.db.assoc.User
            .findAll({
                attributes: [
                    "id",
                    "name",
                    [this.db.sequelize.fn("CONCAT", this.db.sequelize.col("User.id"), " - ", this.db.sequelize.col("User.name")), "nameTag"]
                ],
                include: [
                    {
                        model: this.db.assoc.TaskAssignment,
                        attributes: [],
                        where: {
                            RefUserPrivilegeId: 3,
                            TaskId: taskId
                        }
                    }
                ],
                order: [
                    ["id", "ASC"]
                ]
            })
            .catch(e => { this.devModeLogging("file:task.module func:getTask var:members", e); return []; });

        // (DB-R) Get Task Status Log from DB using taskId
        let taskLog = await this.db.assoc.TaskLog
            .findAll({
                attributes: [
                    "id",
                    "timestampLogged"
                ],
                include: [
                    {
                        model: this.db.assoc.RefTaskStatus,
                        attributes: [
                            "id",
                            "description"
                        ]
                    },
                    {
                        model: this.db.assoc.User,
                        attributes: [
                            "id",
                            "name"
                        ]
                    }
                ],
                where: {
                    TaskId: taskId
                },
                order: [
                    ["id", "DESC"]
                ]
            })
            .catch(e => { this.devModeLogging("file:task.module func:getTask var:taskLog", e); return []; });


        return {
            statusCode: 200,
            data: {
                task: task,
                reviewers: reviewers,
                members: members,
                taskLogs: taskLog
            }
        };
    };

    // Create Task
    /**
     * 
     * @param {Object} param0 
     * @param {Int} param0.projectId 
     * @param {Date} param0.dateStart 
     * @param {Date} param0.dateEnd 
     * @param {String} [param0.category] 
     * @param {String} param0.title 
     * @param {String} param0.description 
     * @param {Int} param0.level 
     * @param {Int} [param0.assignedUserId] 
     * @param {Array} [param0.reviewerArr] 
     * @param {Int} [param0.relatedTaskId] 
     * @returns 
     */
    async createTask({
        projectId,
        dateStart,
        dateEnd,
        category,
        title,
        description,
        level,
        assignedUserId,
        reviewerArr,
        relatedTaskId,
        decoded
    }) {
        if (![2].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        let errorArr = [];

        if (parseInt(level) < 1 || parseInt(level) > 5 || !parseInt(level)) {
            return {
                statusCode: 400,
                error: "Invalid Level"
            };
        }
        level = parseInt(level);

        // Modelling data for input DB + Generate ID
        if (!!reviewerArr) {
            try {
                if (!Array.isArray(reviewerArr)) {
                    reviewerArr = JSON.parse(reviewerArr);
                }
            }
            catch (e) {
                return {
                    statusCode: 400,
                    error: "Invalid value for reviewerArr"
                };
            }
        }

        if (!!relatedTaskId) {
            // (DB-R) Check if relatedTaskId is Exists (and not hidden) and in same project
            let relatedTask = await this.db.assoc.Task
                .count({
                    where: {
                        isHidden: false,
                        id: relatedTaskId,
                        ProjectId: projectId
                    }
                })
                .catch(e => { this.devModeLogging("file:task.module func:createTask var:relatedTask", e); return false; });

            if (!relatedTask) {
                errorArr.push(`Can't find taskId: ${relatedTaskId} from relatedTaskId in this project`);
                relatedTaskId = undefined;
            }
        }

        // (DB-C) Insert Task to DB
        let insertTask = await this.db.assoc.Task
            .create({
                createdByUserId: decoded.userId,
                ProjectId: projectId,
                category: category,
                title: title,
                description: description,
                timestampStartPlan: dateStart,
                timestampEndPlan: dateEnd,
                RefTaskLevelId: level,
                relatedTaskId: relatedTaskId,
                RefTaskStatusId: 1
            })
            .catch(e => { this.devModeLogging("file:task.module func:createTask var:insertTask", e, true); return false; });

        if (!insertTask) {
            return {
                statusCode: 400,
                error: "Incorrect values"
            };
        }

        // (DB-C) Insert Log to tb_task_log
        let insertLog = await this.db.assoc.TaskLog
            .create({
                TaskId: insertTask.id,
                UserId: decoded.userId,
                RefTaskStatusId: 1
            })
            .catch(e => { this.devModeLogging("file:task.module func:createTask var:insertLog", e, true); return false; });

        if (!!assignedUserId) {
            // (DB-R) Check if user is project member
            let member = await this.db.assoc.ProjectAssignment
                .findOne({
                    attributes: [
                        "UserId"
                    ],
                    where: {
                        ProjectId: projectId,
                        UserId: assignedUserId,
                        RefUserPrivilegeId: 3
                    }
                })
                .catch(e => { this.devModeLogging("file:task.module func:createTask var:member", e, true); return false; });

            if (!!member) {
                // (DB-C) Insert assigned user to DB
                let insertAssigned = await this.db.assoc.TaskAssignment
                    .create({
                        TaskId: insertTask.id,
                        UserId: assignedUserId,
                        RefUserPrivilegeId: 3
                    })
                    .catch(e => { this.devModeLogging("file:task.module func:createTask var:insertAssigned", e, true); return false; });

                if (!insertAssigned) {
                    errorArr.push(`Cannot add user as assigned user. userId: ${assignedUserId}`);
                }
            }
            else {
                errorArr.push(`Cannot add user as assigned user. userId: ${assignedUserId}`);
            }
        }

        if (!!reviewerArr) {
            for (let reviewerId of reviewerArr) {
                // (DB-R) Check if reviewerId is member of project
                let reviewer = await this.db.assoc.ProjectAssignment
                    .findOne({
                        attributes: [
                            "UserId"
                        ],
                        where: {
                            ProjectId: projectId,
                            UserId: reviewerId,
                            RefUserPrivilegeId: 2
                        }
                    })
                    .catch(e => { this.devModeLogging("file:task.module func:createTask var:reviewer", e, true); return false; });


                if (!reviewer) {
                    errorArr.push(`Cannot add user as reviewer. userId: ${reviewerId}`);
                    continue;
                }

                // (DB-C) Insert Reviewer to DB
                let insertReviewer = await this.db.assoc.TaskAssignment
                    .create({
                        TaskId: insertTask.id,
                        UserId: reviewerId,
                        RefUserPrivilegeId: 2
                    })
                    .catch(e => { this.devModeLogging("file:task.module func:createTask var:insertReviewer", e, true); return false; });


                if (!insertReviewer) {
                    errorArr.push(`Can't set userId ${reviewerId} into reviewer. Please re-assign manually`);
                }
            }
        }

        // (DB-R) Set Project to unfinished
        let setUnfinished = await this.db.assoc.Project
            .update(
                {
                    timestampFinished: null
                },
                {
                    where: {
                        id: projectId
                    }
                }
            ).catch(e => { consdevModeLogging("file:task.module func:createTask var:setUnfinished", e, true); });

        return {
            statusCode: 200,
            data: "Task created successfully",
            error: errorArr.length > 0 ? errorArr : undefined
        };
    };

    // Edit Task
    /**
     * 
     * @param {Object} param0 
     * @param {Int} param0.taskId 
     * @param {Date} [param0.dateStart] 
     * @param {Date} [param0.dateEnd] 
     * @param {String} [param0.category] 
     * @param {String} [param0.title] 
     * @param {String} [param0.description] 
     * @param {Int} [param0.level] 
     * @param {Int} [param0.assignedUserId] 
     * @param {Array} [param0.reviewerArr] 
     * @param {Boolean} [param0.isReportAccepted] - Leave Blank for non-Report
     * @param {Int} [param0.relatedTaskId] 
     * @returns 
     */
    async editTask({
        taskId,
        dateStart,
        dateEnd,
        category,
        title,
        description,
        level,
        assignedUserId,
        reviewerArr = [],
        relatedTaskId,
        isReportAccepted,
        decoded
    }) {
        if (![2].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Check if Editting from status REPORT
        let taskReport = await this.db.assoc.Task
            .count({
                where: {
                    id: taskId,
                    RefTaskStatusId: 5
                }
            })
            .catch(e => { this.devModeLogging("file:task.module func:editTask var:taskReport", e); return 0; });

        if (!!taskReport && ![0, 1].includes(parseInt(isReportAccepted))) {
            return {
                statusCode: 400,
                error: "Can't Edit Task while on REPORT status/isReportAccepted only accept 0/1/NULL value"
            };
        }

        // (DB-R) Get This Task
        let thisTask = await this.db.assoc.Task
            .findOne({
                where: {
                    id: taskId
                }
            })
            .catch(e => { this.devModeLogging("file:task.module func:editTask var:thisTask", e, true); return false; });


        let errorArr = [];

        // Prepare for the Set
        let updObj = {};
        if (!!dateStart && dateStart.length > 0) updObj.timestampStartPlan = dateStart;
        if (!!dateEnd && dateEnd.length > 0) updObj.timestampEndPlan = dateEnd;
        if (!!category && category.length > 0) updObj.category = category;
        if (!!title && title.length > 0) updObj.title = title;
        if (!!description && description.length > 0) updObj.description = description;
        if (!!level) {
            try {
                level = level.toString();
                if (!(1 <= parseInt(level) && parseInt(level) <= 5)) {
                    errorArr.push("level is not valid. Require: Integer");
                }
                level = parseInt(level);
                updObj.RefTaskLevelId = level;
            }
            catch (e) {
                this.devModeLogging("file:task.module func:editTask var:level", e, true);
            }
        }
        if (!!relatedTaskId) {
            relatedTaskId = parseInt(relatedTaskId);

            // (DB-R) Check if relatedTaskId is Exists and in same project
            let relatedTask = await this.db.assoc.Task
                .count({
                    where: {
                        id: relatedTaskId,
                        ProjectId: thisTask.ProjectId
                    }
                })
                .catch(e => { this.devModeLogging("file:task.module func:editTask var:relatedTask", e); return false; });

            if (!relatedTask) {
                errorArr.push(`Can't find taskId: ${relatedTaskId} from relatedTaskId in this project`);
                relatedTaskId = undefined;
            }
            else {
                try {
                    updObj.relatedTaskId = relatedTaskId;
                }
                catch (e) {
                    this.devModeLogging("file:task.module func:editTask var:relatedTaskId", e, true);
                }
            }
        }


        if (updObj != {}) {
            // (DB-U) Edit Task from DB using taskId
            let taskUpdate = await this.db.assoc.Task
                .update(
                    updObj,
                    {
                        where: {
                            isHidden: false,
                            id: taskId
                        }
                    }
                )
                .catch(e => { this.devModeLogging("file:task.module func:editTask var:taskUpdate", e, true); return false; });

            if (!taskUpdate) {
                return {
                    statusCode: 400,
                    data: "Invalid input/Task already removed"
                };
            }

            // (DB-C) Insert edit log
            let insertLog = await this.db.assoc.TaskLog
                .create({
                    TaskId: taskId,
                    UserId: decoded.userId,
                    RefTaskStatusId: thisTask.RefTaskStatusId,
                    isEdit: true
                })
                .catch(e => { this.devModeLogging("file:task.module func:editTask var:insertLog", e, true); });
        }

        if (!!assignedUserId) {
            // (DB-R) Check if user is project member
            let member = await this.db.assoc.ProjectAssignment
                .count({
                    include: [
                        {
                            model: this.db.assoc.Project,
                            where: {
                                id: thisTask.ProjectId
                            }
                        }
                    ],
                    where: {
                        UserId: assignedUserId,
                        RefUserPrivilegeId: 3
                    }
                })
                .catch(e => { this.devModeLogging("file:task.module func:editTask var:member", e, true); return false; });

            if (!!member) {
                // (DB-U) Change userId of Assigned User
                let updateAssigned = await this.db.assoc.TaskAssignment
                    .update(
                        {
                            UserId: assignedUserId
                        },
                        {
                            where: {
                                TaskId: taskId,
                                RefUserPrivilegeId: 3
                            }
                        }
                    )
                    .catch(e => { this.devModeLogging("file:task.module func:editTask var:updateAssigned", e, true); return false; });

                // No member assigned before
                if (updateAssigned[0] == 0) {
                    // (Db-C) Insert Assigned User
                    updateAssigned = await this.db.assoc.TaskAssignment
                        .create({
                            TaskId: taskId,
                            UserId: assignedUserId,
                            RefUserPrivilegeId: 3
                        })
                        .catch(e => { this.devModeLogging("file:task.module func:editTask var:insertAssigned", e, true); return false; });
                }

                if (updateAssigned[0] == 0) {
                    errorArr.push(`Cannot update user as assigned user. userId: ${assignedUserId}`);
                }
            }
            else {
                errorArr.push(`Cannot update user as assigned user. userId: ${assignedUserId}`);
            }
        }

        if (!!reviewerArr && reviewerArr.length > 0) {
            try {
                if (!Array.isArray(reviewerArr)) {
                    reviewerArr = JSON.parse(reviewerArr);
                }
            }
            catch (e) {
                return {
                    statusCode: 400,
                    error: "Invalid reviewerArr"
                };
            }

            // (DB-R) Checking Reviewer
            let reviewers = await this.db.assoc.TaskAssignment
                .findOne({
                    attributes: [
                        [this.db.sequelize.fn("array_agg", this.db.sequelize.col("UserId")), "userIds"],
                    ],
                    where: {
                        TaskId: taskId,
                        RefUserPrivilegeId: 2
                    }
                })
                .catch(e => { this.devModeLogging("file:task.module func:editTask var:reviewers", e, true); return false; });

            // Check if has different userId
            if (!!reviewers) {
                reviewers = JSON.parse(JSON.stringify(reviewers));

                try {
                    reviewers.userIds.sort();
                }
                catch (e) {
                    if (reviewers.userIds == null) reviewers.userIds = [];
                }

                try {
                    reviewerArr.sort();
                }
                catch (e) { }

                // Check if has different userId
                let toRemove = reviewers.userIds.filter(_rev => !reviewerArr.includes(_rev));
                let toAdd = new Set(reviewerArr.filter(_rev => !reviewers.userIds.includes(_rev)));

                // Update Reviewer
                if (toRemove.length > 0) {
                    // (DB-D) Remove Reviewer
                    let removeReviewer = await this.db.assoc.TaskAssignment
                        .destroy({
                            where: {
                                TaskId: taskId,
                                RefUserPrivilegeId: 2,
                                UserId: {
                                    [this.db.Op.in]: toRemove
                                }
                            }
                        })
                        .catch(e => { this.devModeLogging("file:task.module func:editTask var:removeReviewer", e, true); });
                }

                for (let reviewerId of toAdd) {
                    // (DB-R) Check if reviewerId is member of project
                    let reviewer = await this.db.assoc.ProjectAssignment
                        .count({
                            where: {
                                UserId: reviewerId,
                                RefUserPrivilegeId: 2,
                                ProjectId: thisTask.ProjectId
                            }
                        })
                        .catch(e => { this.devModeLogging("file:task.module func:editTask var:reviewer", e); return false; });

                    if (!reviewer) {
                        toAdd.delete(reviewerId);
                        errorArr.push(`Cannot add user as reviewer. userId: ${reviewerId}`);
                    }

                    // (DB-C) Add Reviewer
                    let addReviewer = await this.db.assoc.TaskAssignment
                        .create({
                            TaskId: taskId,
                            RefUserPrivilegeId: 2,
                            UserId: reviewerId
                        })
                        .catch(e => { this.devModeLogging("file:task.module func:editTask var:addReviewer", e, true); });
                }
            }
        }

        if (!!taskReport) {
            await this.changeTaskStatus({
                taskId: taskId,
                statusId: 1,
                isReportAccepted: isReportAccepted,
                decoded: decoded
            });
        }


        return {
            statusCode: 200,
            data: "Task Edited",
            errorArr: (errorArr.length > 0 ? errorArr : undefined)
        };
    };

    // Remove Task by Hiding it
    /**
     * 
     * @param {Object} param0 
     * @param {Int} param0.taskId
     * @returns 
     */
    async removeTask({
        taskId,
        decoded
    }) {

        if (![2].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Get projectId from taskId
        let project = await this.db.assoc.Task
            .findOne({
                where: {
                    id: taskId
                }
            })
            .catch(e => { this.devModeLogging("file:task.module func:removeTask var:project", e, true); return false; });


        if (!project) {
            return {
                statusCode: 400,
                data: "Task not found"
            };
        }

        // (DB-U) Set Task to Hidden
        let setHidden = await this.db.assoc.Task
            .update(
                {
                    RefTaskStatusId: 8,
                    isHidden: true
                },
                {
                    where: {
                        id: taskId
                    }
                }
            )
            .catch(e => { this.devModeLogging("file:task.module func:removeTask var:setHidden", e, true); });

        // (DB-C) Insert Log to tb_task_log
        let insertLog = await this.db.assoc.TaskLog
            .create({
                TaskId: taskId,
                RefTaskStatusId: 8,
                UserId: decoded.userId
            })
            .catch(e => { this.devModeLogging("file:task.module func:removeTask var:insertLog", e, true); });

        // Check if Project is finished
        if (await this.isProjectFinished({ projectId: project.ProjectId })) {
            // (DB-U) Update Project into Finished
            let setFinished = await this.db.assoc.Project
                .update({
                    timestampEndActual: this.db.sequelize.literal("CURRENT_TIMESTAMP")
                })
                .catch(e => { this.devModeLogging("file:task.module func:removeTask var:setFinished", e, true); });
        }


        return {
            statusCode: 200,
            data: "Task Removed"
        };
    };



    // Task Category
    /**
     * 
     * @param {Object} param0 
     * @param {Int} [param0.projectId] 
     * @param {String} [param0.searchStr] 
     * @param {Object} param0.decoded 
     */
    async taskCategory({
        projectId,
        searchStr = "",
        decoded
    }) {
        if (![2].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Find Task Categories from DB
        let condition = {
            category: {
                [this.db.Op.and]: {
                    [this.db.Op.not]: null,
                    [this.db.Op.iLike]: "%" + searchStr + "%" || "%%"
                }
            },
        };
        if (!isNaN(projectId)) condition.ProjectId = projectId;
        let categories = await this.db.assoc.Task
            .findOne({
                attributes: [
                    [this.db.sequelize.fn("array_agg", this.db.sequelize.col("category")), "arr"],
                ],
                where: condition
            })
            .catch(e => { this.devModeLogging("file:task.module func:taskCategory var:categories", e, true); return []; });
        categories = JSON.parse(JSON.stringify(categories));


        return {
            statusCode: 200,
            data: [...new Set(categories.arr)]
        };
    }

    // Monitoring Task
    /**
     * 
     * @param {Object} param0 
     * @param {Int} [param0.memberId]
     * @param {Int} [param0.projectId]
     * @param {Int} [param0.page=1]
     * @param {Int} [param0.resPerPage=10]
     * @param {Boolean} [param0.noPaginate=false]
     * @param {Object} param0.decoded
     * @returns 
     */
    async monitoring({
        memberId,
        projectId,
        page = 1,
        resPerPage = 10,
        noPaginate = false,
        decoded
    }) {
        if (![1, 2].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        let tasks;

        if (!!memberId || !!projectId) {
            // (DB-R) Get list of task from all project from DB
            tasks = await this.db.assoc.Task
                .findAll({
                    attributes: [
                        "id",
                        "title",
                        "category",
                        "timestampCreated",
                        "timestampStartPlan",
                        "timestampEndPlan",
                        "timestampExtendedEndPlan",
                        "timestampStartActual",
                        "timestampEndActual"
                    ],
                    include: [
                        {
                            model: this.db.assoc.RefTaskLevel,
                            attributes: [
                                "id",
                                "description"
                            ],
                        },
                        {
                            model: this.db.assoc.Project,
                            attributes: [
                                "id",
                                "name"
                            ],
                            include: (() => {
                                let obj = [];

                                if ([2].includes(decoded.role)) {
                                    obj.push({
                                        model: this.db.assoc.ProjectAssignment,
                                        attributes: [],
                                        where: (() => {
                                            let obj = {
                                                UserId: decoded.userId
                                            };
                                            return obj;
                                        })(),
                                        required: true
                                    });
                                }

                                return obj;
                            })(),
                            where: (() => {
                                let obj = {
                                    timestampFinished: null
                                };
                                if (!!parseInt(projectId)) obj.id = projectId;
                                return obj;
                            })(),
                            required: true
                        },
                        {
                            model: this.db.assoc.User,
                            as: "CreatorUser",
                            attributes: [
                                "id",
                                "name"
                            ],
                            include: [
                                {
                                    model: this.db.assoc.RefDivision,
                                    attributes: [
                                        "id",
                                        "description"
                                    ],
                                },
                            ],
                            where: (() => {
                                let obj = {};
                                if (!!parseInt(memberId)) obj.id = memberId;
                                return obj;
                            })()
                        },
                        {
                            model: this.db.assoc.TaskAssignment,
                            attributes: [],
                            where: (() => {
                                let obj = {
                                    RefUserPrivilegeId: 3
                                };
                                return obj;
                            })()
                        },
                    ],
                    where: (() => {
                        let obj = {
                            isHidden: false
                        };
                        return obj;
                    })(),
                    group: [],
                    order: [
                        ["id", "DESC"]
                    ],
                    raw: true
                })
                .catch(e => { this.devModeLogging("file:task.module func:monitoring var:tasks IF", e); return []; });
        }
        else {
            tasks = await this.db.assoc.Project
                .findAll({
                    attributes: [
                        "id",
                        "name"
                    ],
                    include: [
                        {
                            model: this.db.assoc.Task,
                            attributes: [
                                [this.db.sequelize.fn("MIN", this.db.sequelize.col(`timestampStartPlan`)), "timestampStartPlan"],
                                [this.db.sequelize.fn("GREATEST",
                                    this.db.sequelize.fn("MAX", this.db.sequelize.col('timestampEndPlan')),
                                    this.db.sequelize.literal(
                                        `CASE
                                            WHEN MAX("Tasks"."timestampExtendedEndPlan") != NULL
                                                THEN MAX("Tasks"."timestampExtendedEndPlan")
                                            ELSE
                                                '-infinity'
                                        END`
                                    )
                                ), "timestampEndPlan"],
                            ],
                            where: {},
                            required: false
                        },
                        {
                            model: this.db.assoc.ProjectAssignment,
                            attributes: [],
                            where: (() => {
                                let obj = {};
                                if ([2].includes(decoded.role)) obj.UserId = decoded.userId;
                                return obj;
                            })(),
                            required: [1].includes(decoded.role) ? false : true
                        }
                    ],
                    where: (() => {
                        let obj = {};
                        if ([2].includes(decoded.role)) obj.timestampFinished = null;
                        return obj;
                    })(),
                    group: [
                        "Project.id",
                    ],
                    order: [
                        ["id", "DESC"]
                    ],
                    raw: true
                })
                .catch(e => { this.devModeLogging("file:task.module func:monitoring var:tasks ELSE", e); return []; });
        }
        tasks = this.dt.transform(tasks);

        // Paginate
        let totalResult = tasks.length;
        let totalPage = 1;
        if (!noPaginate) {
            totalPage = Math.ceil(tasks.length / resPerPage);
            tasks = tasks.splice((page - 1) * resPerPage);
            tasks.splice(resPerPage);
        }

        return {
            statusCode: 200,
            data: tasks,
            maxPage: totalPage,
            totalResult: totalResult
        };
    };

    // Get Task Statuses
    /**
     * 
     * @param {Object} param0 
     * @param {Object} param0.decoded 
     * @returns 
     */
    async getTaskStatus({
        decoded
    }) {
        if (![2, 3].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Get Task Statuses from DB
        let taskStatus = await this.db.assoc.RefTaskStatus
            .findAll({
                attributes: [
                    "id",
                    "description"
                ]
            })
            .catch(e => { this.devModeLogging("file:task.module func:getTaskStatus var:taskStatus", e); return []; });


        return {
            statusCode: 200,
            data: taskStatus
        };
    };

    // Get Simplified Task List (For Related Task)
    /**
     * 
     * @param {Object} param0 
     * @param {Int} param0.projectId 
     * @param {Object} param0.decoded 
     * @returns 
     */
    async getSimplifiedTaskList({
        projectId,
        decoded
    }) {
        if (![2, 3].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Check if user can access the project
        let project = await this.db.assoc.Project
            .count({
                include: [
                    {
                        model: this.db.assoc.ProjectAssignment,
                        attributes: [],
                        where: {
                            UserId: decoded.userId,
                            RefUserPrivilegeId: decoded.role
                        }
                    }
                ],
                where: {
                    id: projectId
                }
            })
            .catch(e => { this.devModeLogging("file:task.module func:getSimplifiedTaskList var:project", e); return 0; });

        if (!project) {
            return {
                statusCode: 403,
                error: "Forbidden to access the project"
            };
        }

        // (DB-R) Get simplified Task list
        let tasks = await this.db.assoc.Task
            .findAll({
                attributes: [
                    "id",
                    "title"
                ],
                include: [
                    {
                        model: this.db.assoc.TaskAssignment,
                        attributes: [
                            "RefUserPrivilegeId"
                        ],
                        include: [
                            {
                                model: this.db.assoc.User,
                                attributes: [
                                    "id",
                                    "name"
                                ]
                            }
                        ],
                        where: {
                            RefUserPrivilegeId: 3
                        }
                    },
                ],
                where: {
                    ProjectId: projectId
                }
            })
            .catch(e => { this.devModeLogging("file:task.module func:getSimplifiedTaskList var:tasks", e); return []; });

        return {
            statusCode: 200,
            data: tasks
        };
    }

    // Change Task Status
    /**
     * 
     * @param {Object} param0 
     * @param {Int} param0.taskId 
     * @param {Int} param0.statusId 
     * @param {Int} [param0.relatedTaskId] 
     * @param {String} [param0.extendedEndDate] 
     * @param {Boolean} [param0.isReportAccepted] - Leave Blank for non-Report
     * @param {Object} param0.decoded 
     * @returns 
     */
    async changeTaskStatus({
        taskId,
        statusId,
        relatedTaskId,
        extendedEndDate,
        isReportAccepted,
        decoded
    }) {

        if (![2, 3].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        let errorArr = [];

        if (!taskId || !parseInt(statusId)) {
            return {
                statusCode: 400,
                error: "Missing Parameter"
            };
        }

        statusId = parseInt(statusId);

        // (DB-R) Get Task from DB using taskId
        let task = await this.db.assoc.Task
            .findOne({
                attributes: [
                    "id",
                    "ProjectId",
                    "RefTaskStatusId",
                    "timestampStartActual"
                ],
                include: [
                    {
                        model: this.db.assoc.TaskAssignment,
                        attributes: [
                            "UserId"
                        ],
                        where: {
                            RefUserPrivilegeId: decoded.role,
                            UserId: decoded.userId
                        }
                    },
                    {
                        model: this.db.assoc.RefTaskStatus,
                        attributes: [
                            "changeTo",
                            "RefUserPrivilegeId"
                        ],
                        where: {
                            id: {
                                [this.db.Op.not]: 4
                            }
                        }
                    }
                ],
                where: {
                    id: taskId,
                    isHidden: false
                }
            })
            .catch(e => { this.devModeLogging("file:task.module func:changeTaskStatus var:task", e); return []; });

        if (!task) {
            return {
                statusCode: 400,
                error: "Task not found/Already Done"
            };
        }
        task = JSON.parse(JSON.stringify(task));

        // Check eligibility for changing status
        if (!task.RefTaskStatus.changeTo.includes(statusId) || task.RefTaskStatus.RefUserPrivilegeId != decoded.role) {
            return {
                statusCode: 403,
                error: "Forbidden to change into this status"
            };
        }

        if (!!relatedTaskId && !parseInt(relatedTaskId)) {
            // (DB-R) Check if relatedTaskId is Exists and in same project
            let relatedTask = await this.db.assoc.Task
                .findOne({
                    where: {
                        id: relatedTaskId,
                        isHidden: false,
                        ProjectId: task.ProjectId
                    }
                })
                .catch(e => { this.devModeLogging("file:task.module func:changeTaskStatus var:relatedTask", e); return false; });

            if (!relatedTask) {
                return {
                    statusCode: 400,
                    error: `Can't find taskId: ${relatedTaskId} from relatedTaskId in this project`
                };
            }
        }

        // (DB-U) Update Task Status on DB using taskId
        let updateTask = await this.db.assoc.Task
            .update(
                (() => {
                    let obj = {
                        RefTaskStatusId: statusId,
                        TaskId: relatedTaskId,
                    };
                    if (statusId == 4) obj.timestampEndActual = this.db.sequelize.literal("CURRENT_TIMESTAMP");
                    if (!task.timestampStartActual && statusId == 2) obj.timestampStartActual = this.db.sequelize.literal("CURRENT_TIMESTAMP");
                    if ([5, 7].includes(parseInt(task.taskStatus)) && !!extendedEndDate && extendedEndDate != "") obj.timestampExtendedEndPlan = extendedEndDate;

                    return obj;
                })(),
                {
                    where: {
                        id: taskId
                    }
                }
            )
            .catch(e => { this.devModeLogging("file:task.module func:changeTaskStatus var:updateTask", e, true); });

        if (statusId == 4) {
            // (DB-U) Set timestamp when setting Status to Done
            let setTimestampEndActual = await this.db.assoc.Task
                .update(
                    {
                        timestampEndActual: (() => {
                            let obj = this.db.assoc.TaskLog
                                .findOne({
                                    where: {
                                        RefTaskStatusId: 3,
                                        TaskId: taskId
                                    },
                                    order: [
                                        ["id", "DESC"]
                                    ]
                                });
                            obj = JSON.parse(JSON.stringify(obj));

                            return obj.timestampLogged;
                        })()
                    },
                    {
                        where: {
                            id: taskId
                        }
                    }
                )
                .catch(e => { this.devModeLogging("file:task.module func:changeTaskStatus var:setTimestampFinished", e, true); });
        }

        // (DB-C) Insert Task Status Log to DB
        let insertLog = await this.db.assoc.TaskLog
            .create((() => {
                let obj = {
                    TaskId: taskId,
                    RefTaskStatusId: statusId,
                    UserId: decoded.userId
                };
                if (task.RefTaskStatusId == 5 && !isNaN(parseInt(isReportAccepted))) obj.isReportAccepted = !!parseInt(isReportAccepted);

                return obj;
            })());

        // Check if Project is Finished
        if (await this.isProjectFinished({ taskId: taskId })) {
            // (DB-U) Update Project into Finished
            let updateProject = await this.db.assoc.Project
                .update(
                    {
                        timestampFinished: this.db.sequelize.literal("CURRENT_TIMESTAMP")
                    },
                    {
                        where: {
                            id: task.ProjectId
                        }
                    }
                )
                .catch(e => { this.devModeLogging("file:task.module func:changeTaskStatus var:updateProject", e, true); });
        }


        return {
            statusCode: 200,
            data: "Task Status Changed",
            errorArr: (errorArr.length > 0 ? errorArr : undefined)
        };
    };



    // Check Task status in Project
    /**
     * 
     * @param {Object} param0 
     * @param {Int} param0.taskId 
     * @param {Int} param0.projectId 
     * @returns 
     */
    async isProjectFinished({
        taskId,
        projectId
    }) {
        // (DB-R) Get this Task
        let thisTask = await this.db.assoc.Task
            .findOne({
                where: {
                    id: taskId || 0
                }
            })
            .catch(e => { this.devModeLogging("file:task.module func:isProjectFinished var:thisTask", e, true); return null; });

        // (DB-R) Get total Task
        let allTask = await this.db.assoc.Task
            .count({
                where: {
                    ProjectId: projectId || thisTask.ProjectId
                }
            })
            .catch(e => { this.devModeLogging("file:task.module func:isProjectFinished var:allTask", e, true); return null; });

        // (DB-R) Get total finished Task
        let finsihedTask = await this.db.assoc.Task
            .count({
                where: {
                    ProjectId: projectId || thisTask.ProjectId,
                    RefTaskStatusId: 4
                }
            })
            .catch(e => { this.devModeLogging("file:task.module func:isProjectFinished var:finishedTask", e, true); return null; });

        if (allTask == finsihedTask) {
            return true;
        }

        return false;
    }
}

module.exports = function ({
    dt,
    db,
    devModeLogging
}) {
    return {
        instance: new __task({
            dt: dt,
            db: db,
            devModeLogging: devModeLogging
        }),
        self: __task
    };
};
