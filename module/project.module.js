class __project {
    constructor({
        dt,
        db,
        devModeLogging
    }) {
        this.dt = dt;
        this.db = db;
        this.devModeLogging = devModeLogging;
    }

    // Get Project
    /**
     * 
     * @param {Object} param0 
     * @param {String} [param0.searchStr]
     * @param {Int} [param0.page=1]
     * @param {Int} [param0.resPerPage=10]
     * @param {Boolean} [param0.noPaginate=false]
     * @param {Object} param0.decoded
     * @returns 
     */
    async getProjectList({
        searchStr,
        page = 1,
        resPerPage = 10,
        noPaginate = false,
        decoded
    }) {
        if (![1, 2].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Get list of project from DB
        let projects = await this.db.sequelize.query(
            `SELECT
                "Project"."id",
                "Project"."name",
                "Project"."description",
                "Project"."timestampCreated",
                "Project"."timestampFinished",
                CASE WHEN "Project"."timestampFinished" IS NOT NULL THEN 'Clear' ELSE 'On Progress' END AS projectStatus,
                CASE WHEN "Project"."timestampFinished" IS NOT NULL THEN true ELSE false END AS isDone,
                CASE WHEN "tmp_task"."endDate" < "Project"."timestampFinished" THEN true ELSE false END AS isOverdue
            FROM
                "Projects" AS "Project"
            LEFT JOIN(
                SELECT
                    "Task"."ProjectId",
                    MAX("Task"."timestampEndPlan") AS "endDate"
                FROM
                    "Tasks" AS "Task"
                WHERE
                    "Task"."isHidden" = false
                GROUP BY
                    "Task"."ProjectId"
            ) "tmp_task" ON "tmp_task"."ProjectId" = "Project"."id"
            ${decoded.role == 2 ? 'RIGHT JOIN "ProjectAssignments" AS "ProjectAssignment" ON "ProjectAssignment"."ProjectId" = "Project"."id" ' : ''}
            WHERE
                TRUE
            ${decoded.role == 2 ? 'AND "ProjectAssignment"."UserId" = $userId' : ''}
            ${!!searchStr ? 'AND "Project"."name" LIKE $searchStr' : ''}
            ORDER BY
                "Project"."id" DESC`,
            {
                bind: {
                    searchStr: '%' + searchStr + '%',
                    userId: decoded.userId
                },
                type: this.db.sequelize.QueryTypes.SELECT
            }
        ).catch(e => { this.devModeLogging("file:project.module func:getProjectList var:projects", e); return []; });

        // let projects = await this.db.assoc.ProjectAssignment
        //     .findAll({
        //         attributes: [
        //             "ProjectId"
        //         ],
        //         include: [
        //             {
        //                 model: this.db.assoc.Project,
        //                 attributes: [
        //                     "id",
        //                     "name",
        //                     "description",
        //                     "timestampCreated",
        //                     "timestampFinished",
        //                     [this.db.sequelize.literal(`CASE WHEN "Project"."timestampFinished" IS NOT NULL THEN true ELSE false END`), "isDone"],
        //                     // [this.db.sequelize.literal(`MAX("Project->Tasks"."timestampEndPlan")`), "endPlan"],
        //                     // [this.db.sequelize.literal(`MAX("Project->Tasks"."timestampExtendedEndPlan")`), "extendedEndPlan"],
        //                     // [this.db.sequelize.literal(`CASE WHEN GREATEST(MAX("Project->Tasks"."timestampEndPlan"), MAX("Project->Tasks"."timestampExtendedEndPlan")) < "Project"."timestampFinished" THEN true ELSE false END`), "isOverdue"]
        //                 ],
        //                 include: [
        //                     {
        //                         model: this.db.assoc.Task,
        //                         attributes: [
        //                             // "ProjectId",
        //                             // [this.db.sequelize.fn("GREATEST",
        //                             //     this.db.sequelize.fn("MAX", '"Project->Tasks"."timestampEndPlan"'),
        //                             //     this.db.sequelize.fn("MAX", '"Project->Tasks"."timestampExtendedEndPlan"')
        //                             // ), "endDate"]
        //                         ],
        //                         where: {
        //                             isHidden: false
        //                         },
        //                     }
        //                 ]
        //             }
        //         ],
        //     })
        //     .catch(e => { this.devModeLogging("file:project.module func:getProjectList var:projects", e); return []; });
        projects = JSON.parse(JSON.stringify(projects));

        // Paginate
        let totalResult = projects.length;
        let totalPage = 1;
        if (!noPaginate) {
            totalPage = Math.ceil(projects.length / resPerPage);
            projects = projects.splice((page - 1) * resPerPage);
            projects.splice(resPerPage);
        }

        // (DB-R) Get user list of project from DB
        let users = await this.db.assoc.ProjectAssignment
            .findAll({
                attributes: [
                    "ProjectId",
                    "RefUserPrivilegeId",
                    [this.db.sequelize.fn("array_agg", this.db.sequelize.col("User.id")), "userIds"],
                    [this.db.sequelize.fn("array_agg", this.db.sequelize.col("User.name")), "userNames"]
                ],
                include: [
                    {
                        model: this.db.assoc.User,
                        attributes: []
                    },
                    {
                        model: this.db.assoc.RefUserPrivilege,
                        attributes: []
                    }
                ],
                where: {
                    ProjectId: {
                        [this.db.Op.in]: projects.map(_proj => _proj.id)
                    }
                },
                group: [
                    "ProjectAssignment.ProjectId",
                    "ProjectAssignment.RefUserPrivilegeId"
                ],
                order: [
                    [`ProjectId`, "ASC"],
                    [`RefUserPrivilegeId`, "ASC"]
                ]
            })
            .catch(e => { this.devModeLogging("file:project.module func:getProjectList var:users", e, true); return []; });
        users = JSON.parse(JSON.stringify(users));


        // Modelling data before response
        for (let _proj of projects) {
            try {
                let manId = users.filter(_user => _user.ProjectId == _proj.id && _user.RefUserPrivilegeId == 2)[0].userIds;
                // manId = JSON.parse(manId);
                let manName = users.filter(_user => _user.ProjectId == _proj.id && _user.RefUserPrivilegeId == 2)[0].userNames;
                // manName = JSON.parse(manName);

                _proj.managers = [];
                for (let i = 0; i < manId.length; i++) {
                    _proj.managers.push({
                        userId: manId[i],
                        userName: manName[i]
                    });
                }
            }
            catch (e) {
                _proj.managers = [];
            }

            try {
                let memId = users.filter(_user => _user.ProjectId == _proj.id && _user.RefUserPrivilegeId == 3)[0].userIds;
                // memId = JSON.parse(memId);
                let memName = users.filter(_user => _user.ProjectId == _proj.id && _user.RefUserPrivilegeId == 3)[0].userNames;
                // memName = JSON.parse(memName);

                _proj.members = [];
                for (let i = 0; i < memId.length; i++) {
                    _proj.members.push({
                        userId: memId[i],
                        userName: memName[i]
                    });
                }
            }
            catch (e) {
                _proj.members = [];
            }
        }

        return {
            statusCode: 200,
            data: projects,
            maxPage: totalPage,
            totalResult: totalResult
        };
    };

    /**
     * 
     * @param {Object} param0 
     * @param {String} param0.projectId 
     * @param {Object} param0.decoded
     * @returns 
     */
    async getProject({
        projectId,
        decoded
    }) {
        if (![1, 2].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Get Project from DB using projectId
        let projectCondition = {};
        if (decoded.role == 2) {
            projectCondition.UserId = decoded.userId;
        }
        let project = await this.db.assoc.ProjectAssignment
            .findOne({
                attributes: [
                ],
                include: [
                    {
                        model: this.db.assoc.Project,
                        attributes: [
                            "id",
                            "name",
                            "description",
                            "timestampCreated",
                            "timestampFinished"
                        ],
                        where: {
                            id: projectId
                        }
                    }
                ],
                where: projectCondition
            })
            .catch(e => { this.devModeLogging("file:project.module func:getProject var:project", e); return []; });

        if (!project) {
            return {
                statusCode: 400,
                error: "Project not found"
            };
        }

        // (DB-R) Get manager list from DB using projectId
        let managers = await this.db.assoc.ProjectAssignment
            .findAll({
                attributes: [
                    "id",
                    "RefUserPrivilegeId"
                ],
                include: [
                    {
                        model: this.db.assoc.User,
                        attributes: [
                            "id",
                            "name",
                            [this.db.sequelize.fn("CONCAT", "[", this.db.sequelize.col('"User"."id"'), " - ", this.db.sequelize.col('"User"."name"'), "]"), "nameTag"]
                        ],
                        order: [
                            ["id", "DESC"]
                        ]
                    }
                ],
                where: {
                    RefUserPrivilegeId: 2,
                    ProjectId: projectId
                }
            })
            .catch(e => { this.devModeLogging("file:project.module func:getProject var:managers", e); return []; });

        // (DB-R) Get member list from DB using projectId
        let members = await this.db.assoc.ProjectAssignment
            .findAll({
                attributes: [
                    "id",
                    "RefUserPrivilegeId"
                ],
                include: [
                    {
                        model: this.db.assoc.User,
                        attributes: [
                            "id",
                            "name",
                            [this.db.sequelize.fn("CONCAT", "[", this.db.sequelize.col('"User"."id"'), " - ", this.db.sequelize.col('"User"."name"'), "]"), "nameTag"]
                        ],
                        order: [
                            ["id", "DESC"]
                        ]
                    }
                ],
                where: {
                    RefUserPrivilegeId: 3,
                    ProjectId: projectId
                }
            })
            .catch(e => { this.devModeLogging("file:project.module func:getProject var:members", e); return []; });


        return {
            statusCode: 200,
            data: {
                project: project.Project,
                managers: managers,
                members: members
            }
        };
    };

    // Create Project
    /**
     * 
     * @param {Object} param0 
     * @param {String} param0.title 
     * @param {String} param0.description 
     * @param {Array} [param0.memberArr] 
     * @param {Array} [param0.managerArr] 
     * @returns 
     */
    async createProject({
        title,
        description,
        memberArr = [],
        managerArr = [],
        decoded
    }) {
        if (![1].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        let errorArr = [];

        // Modelling data member for input DB
        try {
            if (!Array.isArray(memberArr)) {
                memberArr = JSON.parse(memberArr);
            }
        }
        catch (e) {
            return {
                statusCode: 400,
                error: "Invalid memberArr"
            };
        }

        try {
            if (!Array.isArray(managerArr)) {
                managerArr = JSON.parse(managerArr);
            }
        }
        catch (e) {
            return {
                statusCode: 400,
                error: "Invalid managerArr"
            };
        }

        // (DB-C) Insert Project to DB
        let insertProject = await this.db.assoc.Project
            .create({
                name: title,
                description: description
            })
            .catch(e => { this.devModeLogging("file:project.module func:createProject var:insertProject", e, true); return false; });

        if (!insertProject) {
            return {
                statusCode: 400,
                error: "Failed to create project"
            };
        }

        // (DB-C) Insert Project Manager to DB
        if (managerArr.length > 0) {
            for (let managerId of managerArr) {
                // (DB-R) Check added user Role as Manager
                let manager = await this.db.assoc.User
                    .count({
                        where: {
                            id: managerId,
                            RefUserPrivilegeId: 2
                        }
                    })
                    .catch(e => { this.devModeLogging("file:project.module func:createProject var:manager", e); return false; });

                if (!manager) {
                    errorArr.push(`Cannot add user as manager project. userId: ${managerId}`);
                    continue;
                }

                // (DB-C) Insert manager
                let insertManager = await this.db.assoc.ProjectAssignment
                    .create({
                        ProjectId: insertProject.id,
                        UserId: managerId,
                        RefUserPrivilegeId: 2
                    })
                    .catch(e => { this.devModeLogging("file:project.module func:createProject var:insertManager", e, true); return false; });
            }
        }

        // (DB-C) Insert Project Member to DB
        if (memberArr.length > 0) {
            for (let memberId of memberArr) {
                // (DB-R) Check added user Role as Member
                let member = await this.db.assoc.User
                    .count({
                        where: {
                            id: memberId,
                            RefUserPrivilegeId: 3
                        }
                    })
                    .catch(e => { this.devModeLogging("file:project.module func:createProject var:member", e); return false; });

                if (!member) {
                    errorArr.push(`Cannot add user as member project. userId: ${memberId}`);
                    continue;
                }

                // (DB-C) Insert Member
                let insertMember = await this.db.assoc.ProjectAssignment
                    .create({
                        ProjectId: insertProject.id,
                        UserId: memberId,
                        RefUserPrivilegeId: 3
                    })
                    .catch(e => { this.devModeLogging("file:project.module func:createProject var:insertMember", e, true); return false; });
            }
        }

        return {
            statusCode: 200,
            data: "Project created successfully",
            error: errorArr.length > 0 ? errorArr : undefined
        };
    };

    // Edit Project
    /**
     * 
     * @param {Object} param0 
     * @param {Int} param0.projectId 
     * @param {String} [param0.title] 
     * @param {String} [param0.description] 
     * @param {Array} [param0.memberArr] 
     * @param {Array} [param0.managerArr] 
     * @param {Object} param0.decoded
     * @returns 
     */
    async editProject({
        projectId,
        title,
        description,
        memberArr = [],
        managerArr = [],
        decoded
    }) {
        if (![1].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // Prepare for the Set
        let errorArr = [];
        let updObj = {};
        if (!!title && title.length > 0) updObj.name = title;
        if (!!description && description.length > 0) updObj.description = description;

        if (updObj != {}) {
            // (DB-U) Update project
            let projectUpdate = await this.db.assoc.Project
                .update(
                    updObj,
                    {
                        where: {
                            id: projectId
                        }
                    }
                )
                .catch(e => { this.devModeLogging("file:project.module func:editProject var:projectUpdate", e, true); return false; });

            if (!projectUpdate) {
                return {
                    statusCode: 400,
                    data: "Missing input"
                };
            }
        }

        if (!!managerArr && managerArr.length > 0) {
            // Modelling data manager for input DB
            try {
                if (!Array.isArray(managerArr)) {
                    managerArr = JSON.parse(managerArr);
                }
            }
            catch (e) {
                this.devModeLogging("file:project.module func:editProject var:managerArr", e, true);
                return {
                    statusCode: 400,
                    error: "Invalid managerArr"
                };
            }

            // (DB-R) Checking managers
            let managers = await this.db.assoc.ProjectAssignment
                .findOne({
                    attributes: [
                        "ProjectId",
                        [this.db.sequelize.fn("array_agg", this.db.sequelize.col("ProjectAssignment.UserId")), "userIds"]
                    ],
                    where: {
                        ProjectId: projectId,
                        RefUserPrivilegeId: 2
                    },
                    group: [
                        "ProjectId"
                    ]
                })
                .catch(e => { this.devModeLogging("file:project.module func:editProject var:managers", e, true); return false; });

            if (!!managers) {
                // managers = managers[0];

                try {
                    // managers.userIds = JSON.parse(managers.userIds);
                    managers = JSON.parse(JSON.stringify(managers));
                    managers.userIds.sort();
                }
                catch (e) {
                    if (managers.userIds == null) managers.userIds = [];
                }

                try {
                    managerArr.sort();
                }
                catch (e) { }

                let toRemove = managers.userIds.filter(_man => !managerArr.includes(_man));
                let toAdd = new Set(managerArr.filter(_man => !managers.userIds.includes(_man)));

                // Update Manager
                if (toRemove.length > 0) {
                    // (DB-D) Remove Manager
                    let removeManager = this.db.assoc.ProjectAssignment
                        .destroy({
                            where: {
                                ProjectId: projectId,
                                RefUserPrivilegeId: 2,
                                UserId: {
                                    [this.db.Op.in]: toRemove
                                }
                            }
                        })
                        .catch(e => { this.devModeLogging("file:project.module func:editProject var:removeManager", e, true); });
                }

                for (let managerId of toAdd) {
                    // (DB-R) Check added user Role as Manager
                    let manager = await this.db.assoc.User
                        .count({
                            where: {
                                id: managerId,
                                RefUserPrivilegeId: 2
                            }
                        })
                        .catch(e => { this.devModeLogging("file:project.module func:editProject var:manager", e); return false; });

                    if (!manager) {
                        errorArr.push(`Cannot add user as manager project. userId: ${managerId}`);
                        continue;
                    }

                    // (DB-C) Add Manager
                    let addManager = await this.db.assoc.ProjectAssignment
                        .create({
                            ProjectId: projectId,
                            UserId: managerId,
                            RefUserPrivilegeId: 2
                        })
                        .catch(e => { this.devModeLogging("file:project.module func:editProject var:addManager", e, true); return false; });
                }
            }
        }


        if (!!memberArr && memberArr.length > 0) {
            // Modelling data member for input DB
            try {
                if (!Array.isArray(memberArr)) {
                    memberArr = JSON.parse(memberArr);
                }
            }
            catch (e) {
                this.devModeLogging("file:project.module func:editProject var:memberArr", e, true);
                return {
                    statusCode: 400,
                    error: "Invalid memberArr"
                };
            }

            // (DB-R) Checking Members
            let members = await this.db.assoc.ProjectAssignment
                .findOne({
                    attributes: [
                        "ProjectId",
                        [this.db.sequelize.fn("array_agg", this.db.sequelize.col("ProjectAssignment.UserId")), "userIds"]
                    ],
                    where: {
                        ProjectId: projectId,
                        RefUserPrivilegeId: 3
                    },
                    group: [
                        "ProjectId"
                    ]
                })
                .catch(e => { this.devModeLogging("file:project.module func:editProject var:members", e, true); return false; });


            if (!!members) {
                // members = members[0];

                try {
                    // members.userIds = JSON.parse(members.userIds);
                    members = JSON.parse(JSON.stringify(members));
                    members.userIds.sort();
                }
                catch (e) {
                    if (members.userIds == null) members.userIds = [];
                }

                try {
                    memberArr.sort();
                }
                catch (e) { }

                // Check if has different userId
                let toRemove = members.userIds.filter(_mem => !memberArr.includes(_mem));
                let toAdd = new Set(memberArr.filter(_mem => !members.userIds.includes(_mem)));

                // Update Members
                if (toRemove.length > 0) {
                    // (DB-D) Remove Member
                    let removeMember = this.db.assoc.ProjectAssignment
                        .destroy({
                            where: {
                                ProjectId: projectId,
                                RefUserPrivilegeId: 3,
                                UserId: {
                                    [this.db.Op.in]: toRemove
                                }
                            }
                        })
                        .catch(e => { this.devModeLogging("file:project.module func:editProject var:removeMember", e, true); });
                }

                for (let memberId of toAdd) {
                    // (DB-R) Check added user Role as Member
                    let member = await this.db.assoc.User
                        .count({
                            where: {
                                id: memberId,
                                RefUserPrivilegeId: 3
                            }
                        })
                        .catch(e => { this.devModeLogging("file:project.module func:editProject var:member", e); return false; });


                    if (!member) {
                        errorArr.push(`Cannot add user as member project. userId: ${memberId}`);
                        continue;
                    }

                    // (DB-C) Add Member
                    let addMember = await this.db.assoc.ProjectAssignment
                        .create({
                            ProjectId: projectId,
                            UserId: memberId,
                            RefUserPrivilegeId: 3
                        })
                        .catch(e => { this.devModeLogging("file:project.module func:editProject var:addMember", e, true); return false; });
                }
            }
        }


        return {
            statusCode: 200,
            data: "Project Edited Successfully",
            error: errorArr.length > 0 ? errorArr : undefined
        };
    };

    // Remove Project
    /**
     * 
     * @param {Object} param0 
     * @param {Int} param0.projectId
     * @param {Object} param0.decoded
     * @returns 
     */
    async removeProject({
        projectId,
        decoded
    }) {
        if (![1].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-D) Remove Project from DB using projectId
        let removeProject = await this.db.assoc.Project
            .destroy({
                where: {
                    id: projectId
                }
            })
            .catch(e => { this.devModeLogging("file:project.module func:removeProject var:removeProject", e, true); });

        return {
            statusCode: 200,
            data: "Project Removed Successfully"
        };
    };
}

module.exports = function ({
    dt,
    db,
    devModeLogging
}) {
    return {
        instance: new __project({
            dt: dt,
            db: db,
            devModeLogging: devModeLogging
        }),
        self: __project
    };
};
