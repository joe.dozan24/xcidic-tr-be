class __superadmin {
    constructor({
        dt,
        db,
        devModeLogging
    }) {
        this.dt = dt;
        this.db = db;
        this.devModeLogging = devModeLogging;
    }



    // Performance Member
    /**
     * 
     * @param {Object} param0 
     * @param {String} param0.lastXDays 
     * @param {Boolean} param0.isOverdue 
     * @param {Object} param0.decoded 
     * @returns 
     */
    async getPerformanceMember({
        lastXDays = 7,
        isOverdue,
        decoded
    }) {
        if (![1].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Get Task from DB full detailed
        let members = await this.db.assoc.TaskAssignment
            .findAll({
                attributes: [],
                include: [
                    {
                        model: this.db.assoc.User,
                        attributes: [
                            "id",
                            "employeeId",
                            "name"
                        ],
                        where: {
                            RefUserPrivilegeId: 3
                        }
                    },
                    {
                        model: this.db.assoc.Task,
                        attributes: [],
                        include: [
                            {
                                model: this.db.assoc.TaskLog,
                                attributes: [],
                                where: {
                                    timestampLogged: {
                                        [this.db.Op.gte]: this.db.sequelize.literal(`CURRENT_DATE + ${parseInt(lastXDays) * -1} * INTERVAL '1 day'`)
                                    }
                                }
                            }
                        ],
                        where: (() => {
                            let obj = {
                                isHidden: false
                            };

                            return obj;
                        })(),
                    },
                ],
                where: {},
                group: [
                    ["User.id", "ASC"]
                ],
                raw: true
            })
            .catch(e => { this.devModeLogging("file:superadmin.module func:getPerformanceMember var:members", e); return []; });
        members = this.dt.transform(members);

        for (let member of members) {
            member.TaskLevel = [];
            for (let level = 1; level <= 5; level++) {
                member.TaskLevel.push({
                    level: level,
                    total: await this.db.assoc.Task
                        .count({
                            include: [
                                {
                                    model: this.db.assoc.TaskAssignment,
                                    attributes: [],
                                    where: {
                                        RefUserPrivilegeId: 3,
                                        UserId: member.User.id
                                    },
                                    require: true
                                }
                            ],
                            where: (() => {
                                let obj = {
                                    RefTaskLevelId: level,
                                    RefTaskStatusId: 4
                                };

                                if (!isNaN(isOverdue)) {
                                    if (!!parseInt(isOverdue)) {
                                        obj.timestampEndActual = {
                                            [this.db.Op.and]: [
                                                {
                                                    [this.db.Op.gt]: this.db.sequelize.col("Task.timestampEndPlan"),
                                                },
                                                {
                                                    [this.db.Op.gt]: this.db.sequelize.col("Task.timestampExtendedEndPlan")
                                                }
                                            ]
                                        };
                                    }
                                    else {
                                        obj.timestampEndActual = {
                                            [this.db.Op.or]: [
                                                {
                                                    [this.db.Op.lte]: this.db.sequelize.col("Task.timestampEndPlan"),
                                                },
                                                {
                                                    [this.db.Op.lte]: this.db.sequelize.col("Task.timestampExtendedEndPlan")
                                                }
                                            ]
                                        };
                                    }
                                }

                                return obj;
                            })()
                        })
                        .catch(e => { this.devModeLogging("file:superadmin.module func:getPerformanceMember var:members.taskLevel " + member.User.Id + "-" + level, e); return 0; })
                }
                );
            }
        }


        return {
            statusCode: 200,
            data: members
        };
    };

    // Performance Project
    /**
     * 
     * @param {Object} param0 
     * @param {Object} param0.decoded 
     * @returns 
     */
    async getPerformanceProject({
        decoded
    }) {
        if (![1].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Get unfinished Projects
        let projects = await this.db.assoc.Project
            .findAll({
                attributes: [
                    "id",
                    "name"
                ],
                where: {
                    timestampFinished: null
                },
            })
            .catch(e => { this.devModeLogging("file:superadmin.module func:getPerformanceProject var:projects", e); return []; });
        projects = JSON.parse(JSON.stringify(projects));


        // Modelling data before response
        for (let project of projects) {
            // (DB-R) Get Status each Project
            project.RefTaskStatus = await this.db.assoc.Task
                .findAll({
                    attributes: [
                        "RefTaskStatus.id",
                        "RefTaskStatus.description",
                    ],
                    include: [
                        {
                            model: this.db.assoc.RefTaskStatus,
                            attributes: [],
                            where: {}
                        }
                    ],
                    where: {
                        timestampEndActual: null,
                        ProjectId: project.id
                    },
                    group: [
                        "RefTaskStatus.id"
                    ],
                    raw: true
                })
                .catch(e => { this.devModeLogging("file:superadmin.module func:getPerformanceProject var:project.RefTaskStatus", e); return []; });
            for (let status of project.RefTaskStatus) {
                // (DB-R) Get Member by Status
                status.User = await this.db.assoc.Task
                    .findAll({
                        attributes: [
                            "TaskAssignments->User.id",
                            "TaskAssignments->User.name",
                            [this.db.sequelize.fn("COUNT", "Task.id"), "totalTask"],
                            [this.db.sequelize.literal(`"TaskAssignments->User->RefUserPrivilege"."id"`), "RefUserPrivilege.id"],
                            [this.db.sequelize.literal(`"TaskAssignments->User->RefUserPrivilege"."description"`), "RefUserPrivilege.description"],
                        ],
                        include: [
                            {
                                model: this.db.assoc.TaskAssignment,
                                attributes: [],
                                include: [
                                    {
                                        model: this.db.assoc.User,
                                        attributes: [],
                                        include: [
                                            {
                                                model: this.db.assoc.RefUserPrivilege,
                                                attributes: [],
                                                include: [],
                                                where: {},
                                                required: true
                                            }
                                        ],
                                        where: {},
                                        required: true
                                    },
                                ],
                                where: {
                                    RefUserPrivilegeId: 3
                                }
                            }
                        ],
                        where: {
                            ProjectId: project.id,
                            RefTaskStatusId: status.id
                        },
                        group: [
                            "TaskAssignments->User.id",
                            "TaskAssignments->User->RefUserPrivilege.id"
                        ],
                        raw: true
                    })
                    .catch(e => { this.devModeLogging("file:superadmin.module func:getPerformanceProject var:status.User", e); return []; });
                status.User = this.dt.transform(status.User);


            }
        }
        // this.devModeLogging("PROJECTS", projects);

        return {
            statusCode: 200,
            data: projects
        };
    };

    // Get Change Password Requests
    /**
     * 
     * @param {Object} param0 
     * @param {String} [param0.searchStr] 
     * @param {Int} [param0.page=1] 
     * @param {Int} [param0.resPerPage=10] 
     * @param {Boolean} [param0.noPaginate=false] 
     * @param {Object} param0.decoded 
     * @returns 
     */
    async getChangePasswordRequests({
        searchStr = "",
        page = 1,
        resPerPage = 10,
        noPaginate = false,
        decoded
    }) {
        if (![1].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Get Change Password requests
        let cpRequests = await this.db.assoc.ForgotPassword
            .findAll({
                attributes: [
                    "id",
                    "isDone",
                    "timestampRequested"
                ],
                include: [
                    {
                        model: this.db.assoc.User,
                        attributes: [
                            "id",
                            "name",
                            "email",
                            "phone"
                        ],
                        where: {
                            name: {
                                [this.db.Op.iLike]: "%" + searchStr + "%"
                            }
                        }
                    }
                ],
                where: {},
                order: [
                    ["isDone", "ASC"],
                    ["id", "DESC"]
                ]
            })
            .catch(e => { this.devModeLogging("file:superadmin.module func:getChangePasswordRequests var:cpRequests", e); return []; });
        cpRequests = JSON.parse(JSON.stringify(cpRequests));

        // Paginate
        let totalResult = cpRequests.length;
        let totalPage = 1;
        if (!noPaginate) {
            totalPage = Math.ceil(cpRequests.length / resPerPage);
            cpRequests = cpRequests.splice((page - 1) * resPerPage);
            cpRequests.splice(resPerPage);
        }


        return {
            statusCode: 200,
            data: cpRequests,
            maxPage: totalPage,
            totalResult: totalResult
        };
    };
}

module.exports = function ({
    dt,
    db,
    devModeLogging
}) {
    return {
        instance: new __superadmin({
            dt: dt,
            db: db,
            devModeLogging: devModeLogging
        }),
        self: __superadmin
    };
};
