const crypto = require("crypto-js");
const passcodeAES = require("../configs/AES-Passcode.json").passcode;

const jwt = require("jsonwebtoken");
const passcodeJWT = require("../configs/JWT-Passcode.json").passcode;

const fs = require('fs');
const Jimp = require('jimp');


class __user {
    constructor({
        dt,
        db,
        devModeLogging
    }) {
        this.dt = dt;
        this.db = db;
        this.devModeLogging = devModeLogging;
    }

    // Testing Create Superadmin Account
    async superadminCreateAccount({
        userName,
        userEmail,
        userPhone,
        userPassword
    }) {
        // (DB-R) Check if already has Superadmin
        let superadmin = await this.db.assoc.User
            .findAndCountAll()
            .catch(e => { this.devModeLogging("file:user.module func:superadminCreateAccount var:superadmin", e); return false; });

        if (!!superadmin.count > 0 || !superadmin) {
            return {
                statusCode: 403,
                error: "Forbidden to create more superadmin using this method"
            };
        }

        // Hash SHA256 using crypto.js
        let passwordSHA256 = crypto.SHA256(userPassword).toString();


        // (DB-C) Insert to DB
        let register = await this.db.assoc.User
            .create({
                employeeId: "11",
                name: userName,
                psw: passwordSHA256,
                email: userEmail,
                phone: userPhone,
                RefUserPrivilegeId: 1
            })
            .catch(e => { this.devModeLogging("file:user.module func:superadminCreateAccount var:register", e, true); return false; });

        if (!register) {
            return {
                statusCode: 500,
                error: "Create Superadmin Account Error"
            };
        }

        return {
            statusCode: 200,
            data: "Created Superadmin Account"
        };
    }



    /**
     * 
     * @param {Object} param0 
     * @param {String} [param0.userEmployeeId] 
     * @param {String} [param0.userDivisionId] 
     * @param {String} param0.userName 
     * @param {String} [param0.userPhone] 
     * @param {String} [param0.userEmail] 
     * @param {String} [param0.userGender="L"] 
     * @param {String} param0.userPassword 
     * @param {Int} param0.userRole 
     * @param {Object} param0.decoded 
     * @returns 
     */
    async registerUser({
        userEmployeeId,
        userDivisionId,
        userName,
        userPhone,
        userEmail,
        userGender = "L",
        userPassword,
        userRole = 3,
        decoded
    }) {
        if (![1].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Check if EmployeeId already Exists
        let user = await this.db.assoc.User
            .findOne({
                attributes: ["employeeId"],
                where: {
                    employeeId: userEmployeeId
                }
            })
            .catch(e => { this.devModeLogging("file:user.module func:registerUser var:user", e); return false; });

        if (!!user) {
            return {
                statusCode: 400,
                error: "Employee ID already exists"
            };
        }

        // Hash SHA256 using crypto.js
        let passwordSHA256 = crypto.SHA256(userPassword).toString();

        // (DB-C) Insert to DB
        let insertUser = await this.db.assoc.User
            .create({
                employeeId: userEmployeeId,
                RefDivisionId: userDivisionId,
                name: userName,
                psw: passwordSHA256,
                email: userEmail,
                phone: userPhone,
                gender: userGender,
                RefUserPrivilegeId: userRole
            })
            .catch(e => { this.devModeLogging("file:user.module func:registerUser var:insertUser", e, true); });

        return {
            statusCode: 200,
            data: "User Account Created"
        };
    }

    // Edit User
    /**
     * 
     * @param {Object} param0 
     * @param {String} param0.userId 
     * @param {String} [param0.userEmployeeId] 
     * @param {String} [param0.userDivisionId] 
     * @param {String} [param0.userName] 
     * @param {String} [param0.userEmail] 
     * @param {String} [param0.userPhone] 
     * @param {String} [param0.userGender="L"] 
     * @param {File} [param0.userPhoto] 
     * @param {Int} [param0.userRole] 
     * @param {String} [param0.userOldPassword]
     * @param {String} [param0.userNewPassword] 
     * @param {Object} param0.decoded 
     * @returns 
     */
    async editUser({
        userId,
        userEmployeeId,
        userDivisionId,
        userName,
        userEmail,
        userPhone,
        userGender,
        userPhoto,
        userRole,
        userOldPassword,
        userNewPassword,
        decoded
    }) {
        if (![1, 2, 3].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Check if EmployeeId already Exists
        let user = await this.db.assoc.User
            .findOne({
                where: {
                    employeeId: userEmployeeId || null
                }
            })
            .catch(e => { this.devModeLogging("file:user.module func:editUser var:user", e); return false; });

        if (!!user) {
            return {
                statusCode: 400,
                error: "Employee ID already exists"
            };
        }

        // Process file
        if (!!userPhoto) {
            let fileBuf = fs.readFileSync(userPhoto.destination + userPhoto.filename);

            let saving;
            try {
                saving = await new Promise((resolve, reject) => {
                    Jimp.read(fileBuf)
                        .then(async img => {
                            img.contain(128, 128);
                            await img.writeAsync(`files/profile_${[1].includes(decoded.role) && decoded.userId != userId && !!userId ? userId : decoded.userId}.png`);
                            resolve(true);
                        })
                        .catch(e => {
                            this.devModeLogging("file:user.module func:editUser var:saving JIMP", e, true);
                            reject(false);
                        });
                });
            }
            catch (e) {
                this.devModeLogging("file:user.module func:editUser var:saving", e, true);
                saving = false;
            }

            if (!saving) {
                return {
                    statusCode: 400,
                    error: "Invalid/unsupported image file"
                };
            }
        }

        // Prepare for the Set
        let updObj = {};
        if (!!userNewPassword && userNewPassword.length > 0) {
            if (!([1].includes(decoded.role) && decoded.userId != userId && !!userId)) {
                // (DB-R) Check if Authorized
                let oldPsw = await this.db.assoc.User
                    .findOne({
                        where: {
                            id: decoded.userId,
                            psw: crypto.SHA256(userOldPassword).toString()
                        }
                    })
                    .catch(e => { this.devModeLogging("file:user.module func:editUser var:oldPsw", e); return []; });

                if (!oldPsw) {
                    return {
                        statusCode: 400,
                        error: "Invalid Password"
                    };
                }
            }

            // updArr.push('tb_user.userPsw = ' + this.mysql.escape(crypto.SHA256(userNewPassword).toString()));
            updObj.psw = crypto.SHA256(userNewPassword).toString();
            if ([1].includes(decoded.role)) {
                // (DB-U) Update forgot password
                let updateFP = await this.db.assoc.ForgotPassword
                    .update({
                        isDone: true
                    },
                        {
                            where: {
                                UserId: ([1].includes(decoded.role) && decoded.userId != userId && !!userId ? userId : decoded.userId)
                            }
                        })
                    .catch(e => { this.devModeLogging("file:user.module func:editUser var:updateFP", e, true); return false; });
            }
        }
        if (!!userEmployeeId && userEmployeeId.length > 0) updObj.employeeId = userEmployeeId;
        if (!!userDivisionId && parseInt(userDivisionId) > 0) updObj.RedDivisionId = userDivisionId;
        if (!!userName && userName.length > 0) updObj.name = userName;
        if (!!userPhone && userPhone.length > 0) updObj.phone = userPhone;
        if (!!userEmail && userEmail.length > 0) updObj.email = userEmail;
        if (!!userGender && userGender.length > 0) updObj.gender = userGender;
        if (!!userRole && userRole.length > 0 && [1].includes(decoded.role) && !isNaN(parseInt(userRole))) updObj.RefUserPrivilegeId = userRole;

        if (updObj != {}) {
            // (DB-U) Update user info to DB
            let usrUpdate = await this.db.assoc.User
                .update(
                    updObj,
                    {
                        where: {
                            id: ([1].includes(decoded.role) && decoded.userId != userId && !!userId ? userId : decoded.userId)
                        }
                    }
                )
                .catch(e => { this.devModeLogging("file:user.module func:editUser var:usrUpdate", e, true); return false; });

            if (!usrUpdate) {
                return {
                    statusCode: 400,
                    data: "Invalid input/Wrong Password"
                };
            }

            // Check if Changing Role
            if (!!userRole && userRole.length > 0 && [1].includes(decoded.role) && !isNaN(parseInt(userRole)) && decoded.userId != userId) {

                // Logout All Token
                await this.logout({
                    global: true,
                    decoded: {
                        userId: userId
                    }
                });

                // (DB-D) Remove from Project
                let removeUser = await this.db.assoc.ProjectAssignment
                    .destroy({
                        where: {
                            UserId: userId
                        }
                    })
                    .catch(e => { this.devModeLogging("file:user.module func:editUser var:removeUser", e, true); });

                // (DB-D) Unassign from Task
                let unassignUser = await this.db.assoc.TaskAssignment
                    .destroy({
                        where: {
                            UserId: userId
                        }
                    })
                    .catch(e => { this.devModeLogging("file:user.module func:editUser var:unassignUser", e, true); });
            }
        }

        return {
            statusCode: 200,
            data: "User Profile Edited Successfully"
        };
    }

    // Hide User (Remove User)
    /**
     * 
     * @param {Object} param0 
     * @param {Int} param0.userId 
     * @param {Object} param0.decoded 
     * @returns 
     */
    async hideUser({
        userId,
        decoded
    }) {
        if (![1].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }


        // (DB-U) Hide User
        let hideUser = await this.db.assoc.User
            .update(
                {
                    employeeId: this.db.sequelize.fn("CONCAT", "REMOVED-", this.db.sequelize.col("employeeId")),
                    isHidden: true
                },
                {
                    where: {
                        id: userId,
                        isHidden: false
                    }
                }
            )
            .catch(e => { this.devModeLogging("file:user.module func:hideUser var:hideUser", e, true); return [0]; });
        this.devModeLogging("HIDEUSER", hideUser);

        if (hideUser[0] == 0) {
            return {
                statusCode: 500,
                error: "Internal Server Error"
            };
        }

        return {
            statusCode: 200,
            data: "User Removed"
        };
    };

    // Login
    /**
     * 
     * @param {Object} param0 
     * @param {String} param0.userEmployeeId
     * @param {String} param0.userPassword
     * @returns 
     */
    async login({
        userEmployeeId,
        userPassword
    }) {
        // (DB-R) Get User from DB
        let user = await this.db.assoc.User
            .findOne({
                attributes: [
                    "id",
                    "name",
                ],
                include: [
                    {
                        model: this.db.assoc.RefUserPrivilege,
                        attributes: [
                            "id",
                            "name",
                            "description"
                        ],
                        required: false
                    }
                ],
                where: {
                    employeeId: userEmployeeId,
                    psw: crypto.SHA256(userPassword).toString(),
                    isHidden: false
                }
            })
            .catch(e => { this.devModeLogging("file:user.module func:login var:user", e); return []; });

        if (user.length < 1) {
            return {
                statusCode: 401,
                error: "Invalid Username/Password"
            };
        }

        user = JSON.parse(JSON.stringify(user));

        // Add timestamp to user data
        const now = Date.now();
        user.now = now;

        // Encrypt using AES
        const tokenAES = crypto.AES.encrypt(JSON.stringify(user), passcodeAES).toString();

        // (DB-C) Insert Token to DB]\
        let insertToken = this.db.assoc.UserLoginToken
            .create({
                UserId: user.id,
                token: tokenAES
            })
            .catch(e => { this.devModeLogging("file:user.module func:login var:insertToken", e); return []; });

        // Encrypt token using JWT
        const tokenJWT = jwt.sign(
            {
                userId: user.id,
                role: user.RefUserPrivilege.id,
                token: tokenAES,
                timestamp: Date.now()
            },
            passcodeJWT,
            {
                expiresIn: "8h"
            }
        );


        return {
            statusCode: 200,
            data: {
                userId: user.id,
                userName: user.name,
                token: tokenJWT,
                roleId: user.RefUserPrivilege.id,
                roleName: user.RefUserPrivilege.description
            }
        };
    };

    // Logout
    /**
     * 
     * @param {Object} param0 
     * @param {Boolean} [param0.global=false] 
     * @param {Object} param0.decoded 
     * @returns 
     */
    async logout({
        global = false,
        decoded
    }) {
        // (DB-U) Update token from DB into Signed Out
        console.log(JSON.stringify(this.db.sequelize.literal("CURRENT_TIMESTAMP")));
        let updateToken = await this.db.assoc.UserLoginToken
            .update(
                {
                    isSignedOut: true,
                    timestampSignedOut: this.db.sequelize.literal("CURRENT_TIMESTAMP")
                },
                {
                    where: {
                        UserId: decoded.userId,
                        [this.db.Op.or]: [
                            {
                                token: !global ? decoded.token : null
                            },
                            {
                                isSignedOut: !!global ? false : null
                            }
                        ]
                    }
                })
            .catch(e => { this.devModeLogging("file:user.module func:logout var:updateToken", e, true); });


        return {
            statusCode: 200,
            data: "Signed Out successfully"
        };
    };



    // Get User Privileges list
    /**
     * 
     * @param {Object} param0 
     * @param {Object} param0.decoded 
     * @returns 
     */
    async getUserPrivilegesList({
        decoded
    }) {
        if (![1].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Get user privileges list from DB
        let userPriv = await this.db.assoc.RefUserPrivilege
            .findAll({
                attributes: [
                    "id",
                    "description"
                ]
            })
            .catch(e => { this.devModeLogging("file:user.module func:getUserPrivilegesList var:userPriv", e); return []; });

        return {
            statusCode: 200,
            data: userPriv
        };
    }

    // Get User Profile
    /**
     * 
     * @param {Object} param0 
     * @param {Int} [param0.userId] 
     * @param {Int} [param0.roleId] 
     * @param {Int} [param0.divisionId] 
     * @param {String} [param0.searchStr] 
     * @param {Int} [param0.page=1] 
     * @param {Int} [param0.resPerPage=10] 
     * @param {Boolean} [param0.noPaginate=false] 
     * @param {Object} param0.decoded 
     * @returns 
     */
    async getUserProfile({
        userId,
        roleId,
        divisionId,
        searchStr,
        page = 1,
        resPerPage = 10,
        noPaginate = false,
        decoded
    }) {
        if (![1, 2, 3].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        // (DB-R) Get user profile without password
        let condition = {};
        if ([1, 2].includes(decoded.role) || !!searchStr || !!roleId || !!divisionId) {
            condition[this.db.Op.and] = [];
            condition[this.db.Op.and].push({
                isHidden: false
            });

            // User
            if (![1].includes(decoded.role)) {
                condition[this.db.Op.and].push({
                    id: decoded.userId
                });
            }
            else {
                if (!!userId) {
                    condition[this.db.Op.and].push({
                        id: userId
                    });
                }
            }

            // Search name
            if (!!searchStr && [1].includes(decoded.role)) {
                condition[this.db.Op.and].push({
                    name: {
                        [this.db.Op.iLike]: "%" + searchStr + "%"
                    }
                });
            }

            // Filter by roleId
            if (!!roleId && [1].includes(decoded.role)) {
                condition[this.db.Op.and].push({
                    RefUserPrivilegeId: roleId
                });
            }
            else {
                if ([1].includes(decoded.role) && (!!userId || !!searchStr)) { }
                else {
                    condition[this.db.Op.and].push({
                        RefUserPrivilegeId: {
                            [this.db.Op.not]: 1
                        }
                    });
                }
            }

            // Filter by divisionId
            if (!!divisionId && [1, 2].includes(decoded.role)) {
                condition[this.db.Op.and].push({
                    RefDivisionId: divisionId
                });
            }
        }

        let users = await this.db.assoc.User
            .findAll({
                attributes: [
                    "id",
                    "employeeId",
                    "name",
                    [this.db.sequelize.fn("CONCAT", "[", this.db.sequelize.col("User.id"), "-", this.db.sequelize.col("User.name"), "]"), "nameTag"],
                    "phone",
                    "email",
                    "gender"
                ],
                include: [
                    {
                        model: this.db.assoc.RefDivision,
                        attributes: [
                            "id",
                            "description"
                        ],
                        required: false
                    },
                    {
                        model: this.db.assoc.RefUserPrivilege,
                        attributes: [
                            "id",
                            "description"
                        ],
                        required: false
                    }
                ],
                where: condition,
                order: [
                    ["id", 'DESC']
                ]
            })
            .catch(e => { this.devModeLogging("file:user.module func:getuserProfile var:users", e, true); return []; });
        users = JSON.parse(JSON.stringify(users));


        // Paginate
        let totalResult = users.length;
        let totalPage = 1;
        if (!noPaginate) {
            totalPage = Math.ceil(users.length / resPerPage);
            users = users.splice((page - 1) * resPerPage);
            users.splice(resPerPage);
        }

        // Append photo profile each user
        for (let user of users) {
            let fileB64;
            try {
                fileB64 = fs.readFileSync(`files/profile_${user.id}.png`).toString('base64');
            }
            catch (e) {
                fileB64 = null; // fs.readFileSync(`files/default.png`).toString('base64');
            }
            user.userPhoto = fileB64;
        }

        return {
            statusCode: 200,
            data: users,
            maxPage: totalPage,
            totalResult: totalResult
        };
    }

    // Remove User Photo
    /**
     * 
     * @param {Object} param0 
     * @param {Int} param0.userId 
     * @param {Object} param0.decoded 
     * @returns 
     */
    async removePhoto({
        userId,
        decoded
    }) {
        if (![1, 2, 3].includes(decoded.role)) {
            return {
                statusCode: 401,
                error: "Unauthorized access"
            };
        }

        try {
            fs.unlinkSync(`files/profile_${[1].includes(decoded.role) && !!userId ? userId : decoded.userId}.png`);
        }
        catch (e) {
            this.devModeLogging("file:user.module func:removePhoto FS_UNLINK", e, true);
            return {
                statusCode: 400,
                error: `Failed to remove photo. userId: ${[1].includes(decoded.role) && !!userId ? userId : decoded.userId}`
            };
        }

        return {
            statusCode: 200,
            data: "Photo successfully removed"
        };
    }

    // Forgot Password
    /**
     * 
     * @param {Object} param0 
     * @param {String} param0.userEmployeeId 
     * @param {String} param0.userName - Deprecated
     * @param {String} param0.userEmail - Deprecated
     * @param {String} param0.userPhone - Deprecated
     * @returns 
     */
    async forgotPassword({
        userEmployeeId
    }) {
        // (DB-R) Look up account
        let user = await this.db.assoc.User
            .findOne({
                attributes: [
                    "id"
                ],
                include: [
                    {
                        model: this.db.assoc.ForgotPassword,
                        attributes: [
                            "isDone"
                        ],
                        order: [
                            ["id", "DESC"]
                        ],
                        where: {
                            isDone: false
                        },
                        required: false
                    }
                ],
                where: {
                    employeeId: userEmployeeId,
                },
            })
            .catch(e => { this.devModeLogging("file:user.module func:forgotPassword var:user", e, true); return []; });
        user = JSON.parse(JSON.stringify(user));

        if (!user) {
            return {
                statusCode: 400,
                error: "Data does not match."
            };
        }

        if (user.ForgotPasswords.length > 0) {
            return {
                statusCode: 400,
                error: "You already requested"
            };
        }

        // (DB-C) Insert Request Forgot Password
        let reqPass = await this.db.assoc.ForgotPassword
            .create({
                UserId: user.id
            })
            .catch(e => { this.devModeLogging("file:user.module func:forgotPassword var:reqPass", e, true); return false; });

        if (!reqPass) {
            return {
                statusCode: 500,
                error: "Internal Server Error"
            };
        }


        return {
            statusCode: 200,
            data: "Reset password request sent"
        };
    }
}

module.exports = function ({
    dt,
    db,
    devModeLogging
}) {
    return {
        instance: new __user({
            dt: dt,
            db: db,
            devModeLogging: devModeLogging
        }),
        self: __user
    };
};