const _devMode = require("./configs/misc.json").devMode;

const express = require("express");
const app = express();

const bparser = require("body-parser");
const multer = require("multer")({ dest: 'tmp/', limits: { fileSize: 5120000 } });

const cors = require("cors");

const dt = require("dottie");


// Dev mode logging
function devModeLogging(_func, arg, bypass = false) {
    if (!(_devMode || bypass)) {
        return;
    }

    console.log("========================");
    console.log(new Date());
    console.log(_func);
    console.log(arg);
    console.log("========================");
}


// Parsing body request
// JSON
app.use(bparser.json());

// application/xwww-form-urlencoded
app.use(bparser.urlencoded({ extended: true }));

// multipart/form-data
app.use(multer.single("file"));
app.use(express.static('public'));

// Port
function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}
const connection = require("./configs/connection.json");
var port = normalizePort(process.env.PORT || connection.api.port);
app.set('port', port);

// CORS
app.use(cors());
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'POST, GET');
        return res.status(200)
            .json({});
    }
    next();
});

let db = require('./class/postgre.class');
let auth = require('./class/auth.class')({
    db: db,
    devModeLogging: devModeLogging
});

// Include Router
const superadminRouter = require("./routes/superadmin.router.js")({
    dt: dt,
    auth: auth,
    db: db,
    devModeLogging: devModeLogging
});
const taskRouter = require("./routes/task.router.js")({
    dt: dt,
    auth: auth,
    db: db,
    devModeLogging: devModeLogging
});
const projectRouter = require("./routes/project.router.js")({
    dt: dt,
    auth: auth,
    db: db,
    devModeLogging: devModeLogging
});
const userRouter = require("./routes/user.router.js")({
    dt: dt,
    auth: auth,
    db: db,
    devModeLogging: devModeLogging
});

// Routing
app.use("/api/superadmin/", superadminRouter);
app.use("/api/task/", taskRouter);
app.use("/api/project/", projectRouter);
app.use("/api/user/", userRouter);

// Start Server
app.listen(port, function () {
    console.log(`[STEPUP] Server listening on port ${port}.`);
});

//* trace unhandledRejection
//* https://stackoverflow.com/questions/43834559/how-to-find-which-promises-are-unhandled-in-node-js-unhandledpromiserejectionwar

process.on("unhandledRejection", (reason, p) => {
    devModeLogging("UNHANDLED_REJECTION", reason, true);
});
