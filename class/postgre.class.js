// from here -> https://codeburst.io/node-js-mysql-and-promises-4c3be599909b
'use strict';
const { Sequelize, DataTypes, Op, QueryTypes } = require('sequelize');
const db_config = require("../configs/db.json");


// Dev mode logging
const _devMode = require("../configs/misc.json").devMode;
function devModeLogging(_func, arg, bypass = false) {
    if (!(_devMode || bypass)) {
        return;
    }

    console.log("========================");
    console.log(new Date());
    console.log(_func);
    console.log(arg);
    console.log("========================");
}

const sq = new Sequelize(db_config.database, db_config.user, db_config.password, {
    host: db_config.host,
    dialect: 'postgres',
    // logging: (...msg) => devModeLogging("POSTGRE_SEQUILIZE", msg)
    logging: false
});

const db = require("./association.class")({
    Op: Op,
    sequelize: sq,
    DataTypes: DataTypes
});

async function createBaseReferences() {
    // User Privilege
    await db.RefUserPrivilege.create({
        name: "SUPERADMIN",
        description: "Superadmin"
    });

    await db.RefUserPrivilege.create({
        name: "MANAGER",
        description: "Manager"
    });

    await db.RefUserPrivilege.create({
        name: "MEMBER",
        description: "Member"
    });

    // Division
    await db.RefDivision.create({
        name: "MECHANICALENGINEERING",
        description: "Mechanical Engineering"
    });

    await db.RefDivision.create({
        name: "SOFTWAREENGGINEERING",
        description: "Software Engineering"
    });

    await db.RefDivision.create({
        name: "BUSINESADMINISTRATION",
        description: "Business Administration"
    });

    // Task Level
    await db.RefTaskLevel.create({
        name: "VERYEASY",
        description: "Very Easy"
    });

    await db.RefTaskLevel.create({
        name: "EASY",
        description: "Easy"
    });

    await db.RefTaskLevel.create({
        name: "MEDIUM",
        description: "Medium"
    });

    await db.RefTaskLevel.create({
        name: "HARD",
        description: "Hard"
    });

    await db.RefTaskLevel.create({
        name: "VERYHARD",
        description: "Very Hard"
    });

    // Task Status
    await db.RefTaskStatus.create({
        name: "TODO",
        description: "To Do",
        changeTo: [2, 5],
        RefUserPrivilegeId: 3
    });

    await db.RefTaskStatus.create({
        name: "ONPROGRESS",
        description: "On Progress",
        changeTo: [3, 5, 6],
        RefUserPrivilegeId: 3
    });

    await db.RefTaskStatus.create({
        name: "WAITINGFORAPPROVAL",
        description: "Waiting for Approval",
        changeTo: [4, 7],
        RefUserPrivilegeId: 2
    });

    await db.RefTaskStatus.create({
        name: "DONE",
        description: "Done",
        changeTo: []
    });

    await db.RefTaskStatus.create({
        name: "REPORT",
        description: "Report",
        changeTo: [1],
        RefUserPrivilegeId: 2
    });

    await db.RefTaskStatus.create({
        name: "PENDING",
        description: "Pending",
        changeTo: [1, 2],
        RefUserPrivilegeId: 3
    });

    await db.RefTaskStatus.create({
        name: "FAILED",
        description: "Failed",
        changeTo: [2],
        RefUserPrivilegeId: 3
    });

    await db.RefTaskStatus.create({
        name: "REMOVED",
        description: "Removed",
        changeTo: []
    });
}

async function pgconnect() {
    try {
        await sq.authenticate();
        console.log("POSTGRE Authenticated");

        let recreate = false;
        await sq.sync({
            alter: true,
            force: recreate
        });

        if (!!recreate) {
            await createBaseReferences();
        }
    }
    catch (e) {
        devModeLogging("POSTGRE_SEQUILIZE Authentication", e, true);
    }
}
pgconnect();

module.exports = {
    QueryTypes: QueryTypes,
    Op: Op,
    DataTypes: DataTypes,
    sequelize: sq,
    assoc: db
};
