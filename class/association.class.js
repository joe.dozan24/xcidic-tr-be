module.exports = function ({
    Op,
    sequelize,
    DataTypes
}) {
    // Import DB Models
    const RefDivision = require('./dbmodel/ref_division.dbmodel')({
        sequelize,
        DataTypes
    });

    const RefTaskLevel = require('./dbmodel/ref_task_level.dbmodel')({
        sequelize,
        DataTypes
    });

    const RefTaskStatus = require('./dbmodel/ref_task_status.dbmodel')({
        sequelize,
        DataTypes
    });

    const RefUserPrivilege = require('./dbmodel/ref_user_privilege.dbmodel')({
        sequelize,
        DataTypes
    });

    const User = require('./dbmodel/user.dbmodel')({
        sequelize,
        DataTypes
    });

    const UserLoginToken = require('./dbmodel/user_login_token.dbmodel')({
        sequelize,
        DataTypes
    });

    const Project = require('./dbmodel/project.dbmodel')({
        sequelize,
        DataTypes
    });

    const ProjectAssignmentLog = require('./dbmodel/project_assignment_log.dbmodel')({
        sequelize,
        DataTypes
    });

    const ProjectAssignment = require('./dbmodel/project_assignment.dbmodel')({
        Op,
        ProjectAssignmentLog,
        sequelize,
        DataTypes
    });

    const Task = require('./dbmodel/task.dbmodel')({
        sequelize,
        DataTypes
    });

    const TaskLog = require('./dbmodel/task_log.dbmodel')({
        sequelize,
        DataTypes
    });

    const TaskAssignmentLog = require('./dbmodel/task_assignment_log.dbmodel')({
        sequelize,
        DataTypes
    });

    const TaskAssignment = require('./dbmodel/task_assignment.dbmodel')({
        Op,
        TaskAssignmentLog,
        sequelize,
        DataTypes
    });

    const ForgotPassword = require('./dbmodel/forgot_password.dbmodel')({
        sequelize,
        DataTypes
    });




    // Association

    // User 1-M RefDivision
    RefDivision.hasMany(User, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    User.belongsTo(RefDivision);

    // User 1-M RefUserPrivilege
    RefUserPrivilege.hasMany(User, {
        type: DataTypes.INTEGER,
        allowNull: false,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    User.belongsTo(RefUserPrivilege);


    // ProjectAssignment 1-M Project
    Project.hasMany(ProjectAssignment, {
        type: DataTypes.INTEGER,
        onDelete: `CASCADE`,
        onUpdate: 'CASCADE'
    });
    ProjectAssignment.belongsTo(Project);

    // ProjectAssignment 1-M User
    User.hasMany(ProjectAssignment, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    ProjectAssignment.belongsTo(User);

    // ProjectAssignment 1-M RefUserPrivilege
    RefUserPrivilege.hasMany(ProjectAssignment, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    ProjectAssignment.belongsTo(RefUserPrivilege);


    // ProjectAssignmentLog 1-M Project
    Project.hasMany(ProjectAssignmentLog, {
        type: DataTypes.INTEGER,
        onDelete: `CASCADE`,
        onUpdate: 'CASCADE'
    });
    ProjectAssignmentLog.belongsTo(Project);

    // ProjectAssignmentLog 1-M User
    User.hasMany(ProjectAssignmentLog, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    ProjectAssignmentLog.belongsTo(User);

    // ProjectAssignmentLog 1-M RefUserPrivilege
    RefUserPrivilege.hasMany(ProjectAssignmentLog, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    ProjectAssignmentLog.belongsTo(RefUserPrivilege);


    // User 1-M RefDivision
    RefDivision.hasMany(User, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    User.belongsTo(RefDivision);

    // User 1-M RefUserPrivilege
    RefUserPrivilege.hasMany(User, {
        type: DataTypes.INTEGER,
        allowNull: false,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    User.belongsTo(RefUserPrivilege);


    // Task 1-M Project
    Project.hasMany(Task, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    Task.belongsTo(Project);

    // Task 1-M User (Created)
    User.hasMany(Task, {
        type: DataTypes.INTEGER,
        foreignKey: "createdByUserId",
        as: "CreatorUser",
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    Task.belongsTo(User, {
        type: DataTypes.INTEGER,
        foreignKey: "createdByUserId",
        as: "CreatorUser",
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });

    // Task 1-M RefTaskLevel
    RefTaskLevel.hasMany(Task, {
        foreignKey: {
            type: DataTypes.INTEGER,
            name: "RefTaskLevelId",
            allowNull: false,
            // defaultValue: 1, // Bugged
        },
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    Task.belongsTo(RefTaskLevel);

    // Task 1-M RefTaskStatus
    RefTaskStatus.hasMany(Task, {
        foreignKey: {
            type: DataTypes.INTEGER,
            name: "RefTaskStatusId",
            allowNull: false,
            // defaultValue: 1, // Bugged
        },
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    Task.belongsTo(RefTaskStatus);

    // Task 1-M Task (RelatedTask)
    Task.hasMany(Task, {
        type: DataTypes.INTEGER,
        foreignKey: "RelatedTaskId",
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    Task.belongsTo(Task, {
        type: DataTypes.INTEGER,
        as: "RelatedTask",
        foreignKey: "RelatedTaskId",
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });


    // TaskLog 1-M Task
    Task.hasMany(TaskLog, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    TaskLog.belongsTo(Task);

    // TaskLog 1-M RefTaskStatus
    RefTaskStatus.hasMany(TaskLog, {
        type: DataTypes.INTEGER,
        defaultValue: 1,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    TaskLog.belongsTo(RefTaskStatus);

    // TaskLog 1-M User
    User.hasMany(TaskLog, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    TaskLog.belongsTo(User);


    // TaskAssignment 1-M Task
    Task.hasMany(TaskAssignment, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    TaskAssignment.belongsTo(Task);

    // TaskAssignment 1-M User
    User.hasMany(TaskAssignment, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    TaskAssignment.belongsTo(User);

    // TaskAssignment 1-M RefUserPrivilege
    RefUserPrivilege.hasMany(TaskAssignment, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    TaskAssignment.belongsTo(RefUserPrivilege);


    // TaskAssignmentLog 1-M Task
    Task.hasMany(TaskAssignmentLog, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    TaskAssignmentLog.belongsTo(Task);

    // TaskAssignmentLog 1-M User
    User.hasMany(TaskAssignmentLog, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    TaskAssignmentLog.belongsTo(User);

    // TaskAssignmentLog 1-M RefUserPrivilege
    RefUserPrivilege.hasMany(TaskAssignmentLog, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    TaskAssignmentLog.belongsTo(RefUserPrivilege);


    // RefTaskStatus 1-M RefUserPrivilege
    RefUserPrivilege.hasMany(RefTaskStatus, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    RefTaskStatus.belongsTo(RefUserPrivilege);


    // UserLoginToken 1-M User
    User.hasMany(UserLoginToken, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    UserLoginToken.belongsTo(User);


    // ForgotPassword 1-M User
    User.hasMany(ForgotPassword, {
        type: DataTypes.INTEGER,
        onDelete: `RESTRICT`,
        onUpdate: 'CASCADE'
    });
    ForgotPassword.belongsTo(User);



    return {
        RefDivision: RefDivision,
        RefTaskLevel: RefTaskLevel,
        RefTaskStatus: RefTaskStatus,
        RefUserPrivilege: RefUserPrivilege,
        User: User,
        UserLoginToken: UserLoginToken,
        Project: Project,
        ProjectAssignment: ProjectAssignment,
        ProjectAssignmentLog: ProjectAssignmentLog,
        Task: Task,
        TaskLog: TaskLog,
        TaskAssignment: TaskAssignment,
        TaskAssignmentLog: TaskAssignmentLog,
        ForgotPassword: ForgotPassword,
    };
};