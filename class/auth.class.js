const jwt = require("jsonwebtoken");
const passcodeJWT = require("../configs/JWT-Passcode.json").passcode;

module.exports = function ({
    db,
    devModeLogging
}) {
    async function validate(token) {
        let decodedToken = await new Promise((resolve, reject) => {
            jwt.verify(token, passcodeJWT, (err, decoded) => {
                if (err) {
                    resolve(false);
                }
                else {
                    resolve(decoded);
                }
            });
        });

        if (decodedToken == false) {
            return false;
        }

        // (DB-R) Check if token still valid
        let isValid = await db.assoc.UserLoginToken
            .findOne({
                include: [
                    {
                        model: db.assoc.User,
                        where: {
                            id: decodedToken.userId,
                            isHidden: false
                        },
                        required: false
                    }
                ],
                where: {
                    token: decodedToken.token,
                    isSignedOut: false
                }
            })
            .catch(e => { devModeLogging("file:auth.class func:validate var:isValid", e); return []; });


        return (!!isValid?.id ? decodedToken : false);
    }

    class Auth {
        async chkToken(req, res, next) {
            let token = req.headers['x-access-token'] || req.headers['authorization'];
            if (!token) {
                res.setHeader('Content-type', 'application/json');
                return res.status(401).json({
                    statusCode: 401,
                    error: "Auth token is not supplied."
                });
            }

            if (token.startsWith('Bearer ')) {
                // Remove Bearer from string
                token = token.slice(7, token.length);
            }

            let decodedToken = await validate(token);

            if (!decodedToken) {
                res.setHeader('Content-type', 'application/json');
                return res.status(401).json({
                    statusCode: 401,
                    error: 'Token is not valid/expired.'
                });
            }

            req.decoded = decodedToken;
            next();
        }
    }

    return new Auth();
};
