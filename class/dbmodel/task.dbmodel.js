class Task {
    constructor({
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("Task", {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            title: {
                type: DataTypes.STRING,
                allowNull: false
            },
            description: {
                type: DataTypes.TEXT,
                allowNull: true
            },
            timestampCreated: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: DataTypes.NOW
            },
            timestampStartPlan: {
                type: DataTypes.DATE,
                allowNull: false,
            },
            timestampEndPlan: {
                type: DataTypes.DATE,
                allowNull: false,
            },
            timestampExtendedEndPlan: {
                type: DataTypes.DATE,
                allowNull: true,
                defaultValue: sequelize.literal("'-infinity'")
            },
            timestampStartActual: {
                type: DataTypes.DATE,
                allowNull: true,
            },
            timestampEndActual: {
                type: DataTypes.DATE,
                allowNull: true,
            },
            category: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            isHidden: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
        });
    }
}

module.exports = function ({
    sequelize,
    DataTypes
}) {
    return (new Task({
        sequelize,
        DataTypes
    })).obj;
};