class TaskAssignmentLog {
    constructor({
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("TaskAssignmentLog", {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            type: {
                type: DataTypes.STRING, // ENUM Error
                values: ["ADD", "REMOVE"],
                allowNull: false,
                defaultValue: "ADD"
            },
            timestampAssigned: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: DataTypes.NOW
            },
        });
    }
}

module.exports = function ({
    sequelize,
    DataTypes
}) {
    return (new TaskAssignmentLog({
        sequelize,
        DataTypes
    })).obj;
};