class ProjectAssignmentLog {
    constructor({
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("ProjectAssignmentLog", {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            type: {
                type: DataTypes.STRING, // ENUM error
                values: ["ADD", "REMOVE"],
                allowNull: false,
                defaultValue: "ADD"
            },
            timestampAssigned: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: DataTypes.NOW
            },
        });
    }
}

module.exports = function ({
    sequelize,
    DataTypes
}) {
    return (new ProjectAssignmentLog({
        sequelize,
        DataTypes
    })).obj;
};