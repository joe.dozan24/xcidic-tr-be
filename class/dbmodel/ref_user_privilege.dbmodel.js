class RefUserPrivilege {
    constructor({
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("RefUserPrivilege", {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            description: {
                type: DataTypes.STRING,
                allowNull: false
            },
        });
    }
}

module.exports = function ({
    sequelize,
    DataTypes
}) {
    return (new RefUserPrivilege({
        sequelize,
        DataTypes
    })).obj;
};