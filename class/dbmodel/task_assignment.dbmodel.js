class TaskAssignment {
    constructor({
        Op,
        TaskAssignmentLog,
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("TaskAssignment",
            {
                id: {
                    type: DataTypes.INTEGER,
                    allowNull: false,
                    primaryKey: true,
                    autoIncrement: true
                },
                timestampAssigned: {
                    type: DataTypes.DATE,
                    allowNull: false,
                    defaultValue: DataTypes.NOW
                },
            },
            {
                hooks: {
                    afterCreate: async (ta, options) => {
                        // (DB-C) Insert to Task Assignment Log
                        await TaskAssignmentLog
                            .create({
                                TaskId: ta.TaskId,
                                UserId: ta.UserId,
                                RefUserPrivilegeId: ta.RefUserPrivilegeId,
                                type: "ADD"
                            });
                    },
                    afterBulkDestroy: async (ta) => {
                        for (let userId of ta.where.UserId[Op.in]) {
                            // (DB-C) Insert to Task Assignment Log
                            await TaskAssignmentLog
                                .create({
                                    TaskId: ta.where.TaskId,
                                    UserId: userId,
                                    RefUserPrivilegeId: ta.where.RefUserPrivilegeId,
                                    type: "REMOVE"
                                });
                        }
                    }
                }
            }
        );
    }
}

module.exports = function ({
    Op,
    TaskAssignmentLog,
    sequelize,
    DataTypes
}) {
    return (new TaskAssignment({
        Op,
        TaskAssignmentLog,
        sequelize,
        DataTypes
    })).obj;
};