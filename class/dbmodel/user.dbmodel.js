class User {
    constructor({
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("User", {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            employeeId: {
                type: DataTypes.STRING,
                allowNull: false
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            phone: {
                type: DataTypes.STRING,
                allowNull: true,
            },
            gender: {
                type: DataTypes.CHAR(1),
                allowNull: false,
                defaultValue: "L"
            },
            psw: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            isHidden: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
        });
    }
}

module.exports = function ({
    sequelize,
    DataTypes
}) {
    return (new User({
        sequelize,
        DataTypes
    })).obj;
};