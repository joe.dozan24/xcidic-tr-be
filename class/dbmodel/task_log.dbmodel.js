class TaskLog {
    constructor({
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("TaskLog", {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            isEdit: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            isReportAccepted: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            },
            timestampLogged: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: DataTypes.NOW
            }
        });
    }
}

module.exports = function ({
    sequelize,
    DataTypes
}) {
    return (new TaskLog({
        sequelize,
        DataTypes
    })).obj;
};