class ForgotPassword {
    constructor({
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("ForgotPassword", {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            isDone: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            timestampRequested: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: DataTypes.NOW
            },
        });
    }
}

module.exports = function ({
    sequelize,
    DataTypes
}) {
    return (new ForgotPassword({
        sequelize,
        DataTypes
    })).obj;
};