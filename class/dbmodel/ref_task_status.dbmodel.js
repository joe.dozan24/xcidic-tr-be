class RefTaskStatus {
    constructor({
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("RefTaskStatus", {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            description: {
                type: DataTypes.STRING,
                allowNull: false
            },
            changeTo: {
                type: DataTypes.JSON,
                allowNull: false,
                defaultValue: []
            },
        });
    }
}

module.exports = function ({
    sequelize,
    DataTypes
}) {
    return (new RefTaskStatus({
        sequelize,
        DataTypes
    })).obj;
};