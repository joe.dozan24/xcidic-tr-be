class RefTaskLevel {
    constructor({
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("RefTaskLevel", {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            description: {
                type: DataTypes.STRING,
                allowNull: false
            },
        });
    }
}

module.exports = function ({
    sequelize,
    DataTypes
}) {
    return (new RefTaskLevel({
        sequelize,
        DataTypes
    })).obj;
};