class RefDivision {
    constructor({
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("RefDivision", {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            description: {
                type: DataTypes.STRING,
                allowNull: false
            },
            isHidden: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
        });
    }
}

module.exports = function ({
    sequelize,
    DataTypes
}) {
    return (new RefDivision({
        sequelize,
        DataTypes
    })).obj;
};