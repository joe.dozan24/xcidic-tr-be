class ProjectAssignment {
    constructor({
        Op,
        ProjectAssignmentLog,
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("ProjectAssignment",
            {
                id: {
                    type: DataTypes.INTEGER,
                    allowNull: false,
                    primaryKey: true,
                    autoIncrement: true
                },
                timestampAssigned: {
                    type: DataTypes.DATE,
                    allowNull: false,
                    defaultValue: DataTypes.NOW
                }
            },
            {
                hooks: {
                    afterCreate: async (pa) => {
                        // (DB-C) Insert to Project Assignment Log
                        await ProjectAssignmentLog
                            .create({
                                ProjectId: pa.ProjectId,
                                UserId: pa.UserId,
                                RefUserPrivilegeId: pa.RefUserPrivilegeId,
                                type: "ADD"
                            });
                    },
                    afterBulkDestroy: async (pa) => {
                        for (let userId of pa.where.UserId[Op.in]) {
                            // (DB-C) Insert to Project Assignment Log
                            await ProjectAssignmentLog
                                .create({
                                    ProjectId: pa.where.ProjectId,
                                    UserId: userId,
                                    RefUserPrivilegeId: pa.where.RefUserPrivilegeId,
                                    type: "REMOVE"
                                });
                        }
                    }
                }
            }
        );
    }
}

module.exports = function ({
    Op,
    ProjectAssignmentLog,
    sequelize,
    DataTypes
}) {
    return (new ProjectAssignment({
        Op,
        ProjectAssignmentLog,
        sequelize,
        DataTypes
    })).obj;
};