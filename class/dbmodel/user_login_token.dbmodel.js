class UserLoginToken {
    constructor({
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("UserLoginToken", {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            token: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            isSignedOut: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            timestampSignedIn: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: DataTypes.NOW
            },
            timestampSignedOut: {
                type: DataTypes.DATE,
                allowNull: true
            },
        });
    }
}

module.exports = function ({
    sequelize,
    DataTypes
}) {
    return (new UserLoginToken({
        sequelize,
        DataTypes
    })).obj;
};