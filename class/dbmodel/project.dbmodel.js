class Project {
    constructor({
        sequelize,
        DataTypes
    }) {
        this.obj = sequelize.define("Project", {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            description: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            timestampCreated: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: DataTypes.NOW
            },
            timestampFinished: {
                type: DataTypes.DATE,
                allowNull: true
            },
        });
    }
}

module.exports = function ({
    sequelize,
    DataTypes
}) {
    return (new Project({
        sequelize,
        DataTypes
    })).obj;
};